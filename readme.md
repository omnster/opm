# Quantum optomechanics

This repository is for lecture notes in Quantum Optomechanics taught in the department of optics, Palacky University in Olomouc.

## Approximate program

1. Kvantová teorie optických procesů v rezonátorech 

2. Optické parametrické oscilátory a zesilovače v rezonátorech. Generace stlačení. 

3. Kvantová teorie tlumení. Heisenberg-Langevinovy rovnice. Markovská approximace. 

4. Mechanické oscilátory a jejich kvantové vlastnosti. 

5. Kvantová teorie optomechanických systémů v rezonátorech.

6. Kvantová teorie elektromechanických systémů. 

7. Pulsní optomechanické a elektromechanické kvantové systémy.
