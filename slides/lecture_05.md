---
title: Quantum Optomechanics. Lecture 5.
subtitle: Quantum Electromechanics.  More on the platforms.  Pulsed Entanglement.
date: 2020-11-04
---

# Introduction

## Optomechanics. Hamiltonian formulation

The system comprises two harmonic oscillators

| Mode      | Bosonic Operator | Frequency    |
| ---       | :---:            | :---:        |
| Radiation | $a$              | $\Omega\s L$ |
| Mechanics | $b$              | $\omega\s m$ |

. . .

The Hamiltonian
\begin{equation}
    \frac 1 \hbar H =  \Omega\s L (x\s m)  a^\da a +  \omega\s m b^\da b
\end{equation}

. . .

The system is parametric, so $\Omega\s L (x \s m) = \Omega\s{ L }(0) + \frac{ \partial }{ \partial x\s m } \Omega\s L \vert_{x\s m = 0} \times x\s m + \dots .$

. . .

After substitution,
\begin{equation}
    \frac 1 \hbar H = \Omega\s L a^\da a + \omega\s m b^\da b - g_0 ( b + b^\da ) a^\da a,
\end{equation}
with notation
\begin{equation}
    \notag
    g_0 :=  - \left. \frac{ \partial \Omega\s L}{ \partial x\s m } \right|_{x\s{m} = 0} \times x\s{ZP},
    \quad
    x\s{ZP} = \sqrt{ \hbar / 2 \mu \omega\s m }.
\end{equation}

## Ways to implement dependence $\Omega\s{L} (x\s m)$

- Change geometry of a cavity
    
    ![Deformed cavities](./fig/fig_5_deform_cavity.png "Change geometry"){ width=50% }
    
- Change optical path inside a cavity

    ![Superfluid Helium Optomechanics](./fig/fig_5_liquid_helium.png "SFH"){ width=30% }
    
## Zoom in superfluid system

![Superfluid Helium Optomechanics](./fig/fig_5_liquid_helium.png "SFH")
    
# Electromechanics


## Quantization of LC-circuit

![](./tikz/lc_circ.pdf "LC circ"){ width=30% }

- Classical energy ($\nu^2 = 1  / LC$)
\begin{equation}
    \mathcal E = \frac{ L I^2(t) }{2 } + \frac{ Q^2 (t) }{ 2 C } = \frac{ L }{ 2 } \left[ I^2 + Q^2 \frac{ 1 }{ L C } \right] = \frac L 2 ( \dot Q^2 + Q^2 \nu^2 ).
\end{equation}

- Ordinary oscillator
\begin{equation}
    \mathcal E = \frac{ m \dot q^2 }{ 2 } + \frac 12 m \omega^2 q^2 = \frac{ m }{ 2 } \left[ \dot q^2 + \omega^2 q^2 \right].
\end{equation}

- Full equivalence
  $Q \mapsto q$ etc. In the end we arrive to the standard Harmonic oscillator Hamiltonian.
  
## Quantization of LC-circuit

- Canonical quadratures
    - charge $Q$ is generalized position
    - magnetic flux $\Phi = LI$ is momentum 
    - from Lagrangian follows $\partial \mathcal L / \partial \dot Q = L I$.
    - commutator $\comm{ Q }{ \Phi } = i \hbar$
- Creation/annihilation operators
\begin{align}
    a & \propto Q + i \Phi,
    \\
    a^\da & \propto Q - i \Phi.
\end{align}
- The Hamiltonian
    $$H = \hbar \nu a^\da a$$
    
## Eletromechanics. System sketch

![Slide by K. Lehnert](./fig/fig_5_lehnert_sketch.png "opt title")

## Electromechanical interaction

Comes from the dependence $C(x)$

- Normally, for a parallel-plate capacitor, with plate distance $d$
\begin{equation}
    C(x) = \frac{ S \varepsilon \varepsilon_0 }{ d } \mapsto 
    \frac{ S \varepsilon \varepsilon_0 }{ d + x } = C_0 \frac{ 1 }{ 1 + \frac x d }
\end{equation}

- Consequently, the circuit frequency
\begin{equation}
    \nu (x) = \frac{ 1 }{ \sqrt{ L C (x) } } = \nu_0 \sqrt{ 1 + \frac x d  } \approx \nu_0 + \nu_0 \frac{ x }{ 2 d }.
\end{equation}

## Electromechanical Hamiltonian

- for two modes, mechanical and microwave

\begin{equation}
    \frac 1 \hbar H = \nu (x) a^\da a  + \omega b^\da b =
    \nu_0 a^\da a + \omega b^\da b - g_0 (b + b^\da) a^\da a,
\end{equation}

- with $g_0 = \frac{ x\s{ZP} }{ 2 d } \nu_0$.
- realistic parameters $x\s{ZP} = 10^{-15}$ m,  $d \sim 10^{-9 }$ so $g_0 \ll \nu_0$.
  

## Equivalence to optomechanics

![Slide by K. Lehnert](./fig/fig_5_lehnert_equiv.png "Equivalence")

## Linearized interaction

- Detuned strong drive at $\omega\s d = \nu_0 - \Delta$
- linearize interaction in the displaced frame
\begin{equation}
    \frac 1 \hbar H = \nu a^\da a + \omega b^\da b - g (a^\da + a )( b^\da + b )
\end{equation}
- in resolved-sideband regime (when $\omega \gg \kappa\s{mw}$), depending on the drive sign
    - $\Delta = + \omega$ then state-swap $H = g( a^\da b + a b^\da)$
    - with $g = x\s{ZP} \sqrt{n\s{cav}} \partial \nu / \partial x$
    - $\Delta = - \omega$ then two-mode squeezing $H =  g (a b + a^\da b^\da)$
    
# Electromechanical entanglement

## Langevin Equations

- Start from the Hamiltonian
\begin{equation}
    \frac 1 \hbar H = \nu a^\da a + \omega b^\da b - g (a^\da b^\da +  a b )
\end{equation}

- Write Langevin equations
\begin{align}
    \dot a & = - \kappa a  - i \nu a  + i g b^\da + \sqrt{ 2 \kappa } a\up{in},
    \\
    \dot b & = - \frac \gamma 2 b  - i \omega b  + i g a^\da + \sqrt{ \gamma } b\up{th}.
\end{align}

## Langevin equations in quadratures

- vector of quadratures $r = (X\s c, Y\s c, X\s m , Y\s m)$.
- Langevin Equations in vector form
\begin{equation}
    \dot r = A r + n,
\end{equation}
$n = (X\up{in}, Y\up{in}, X\up{th}, Y\up{th})$ are the noises

- $A$ is the matrix with coefficients

## Steady state
- Steady state is solution to Lyapunov Equation
\begin{equation}
    A V + V A\up{T} = - D ,
\end{equation}
where $D$ is covariance of noises: $D_{ij} = \frac 12 \avg{ n_i n_j + n_j n_i }$

- Lyapunov equation is a system of linear algebraic equations w.r.t. the 10 elements of covariance matrix

- steady state is also defined by $\dot r = 0$.

- for steady state to exist the system has to be stable ($\Im ( \text{eigenvals} (A)) < 0$).

- Covariance matrix has to be physical
\begin{equation}
    V > 0, \text{ and } V + i \Omega \geq 0.
\end{equation}

- With blue-detuned driving $\Delta = - \omega$, steady state is unphysical unless the driving is very weak.

- Solution: pulsed driving

# Pulsed Entanglement

## Definitions

- The optomechanical interaction $H = g a b + \hc$ is defined by coupling rate $g$.

- The coupling rate $g = g_0 \times \sqrt{ \bar n\s{cav}}$.

- $g_0$ is set by geometry ( $g_0 \propto \partial_x  \Omega\s L$ etc.)

- $\bar n\s{cav}$  is the mean number of intracavity photons due to the coherent driving.
Is determined, obv., by driving.

- If the driving is continuous-wave type,  $\bar n\s{cav} (t) = n\s{c0}$, and consequently $g(t) = \text{const}; \ \dot g = 0$.

- If the driving is pulsed, $g(t) = g,\ 0 < t < \tau$ and $g (t) = 0$ otherwise.

## The strategy with pulsed opto/electromechanics

- Write Langevin equations assuming constant driving 
- Integrate for time $0 < t < \tau$
- That is, obtain input-output relations that connect quantum state at $t = \tau$ with the initial state at $t = 0$.


## Preliminaries

- go to rotating frame defined by $H\s{rf} = \nu a^\da a + \omega b^\da b$ (switch to slow amplitudes $a \mapsto a e^{ - i \nu t }, b\mapsto b e^{ - i \omega t }$)
\begin{equation}
    H = - g ( a^\da b^\da + a b ).
\end{equation}

- Langevin equations
\begin{align}
    \dot a & = - \kappa a  + i g b^\da + \sqrt{ 2 \kappa } a\up{in},
    \\
    \dot b & = - \frac \gamma 2 b + i g a^\da + \sqrt{ \gamma } b\up{th}.
\end{align}

- add input-output relations
\begin{equation}
    a\up{out} = - a\up{in} + \sqrt{ 2 \kappa } a.
\end{equation}

## Approximations

- Langevin equations
\begin{align}
    \dot a & = - \kappa a  + i g b^\da + \sqrt{ 2 \kappa } a\up{in},
    \\
    \dot b & = - \frac \gamma 2 b + i g a^\da + \sqrt{ \gamma } b\up{th}.
\end{align}

- Typical Parameters:
    - $\kappa \sim 300$ kHz,  $g = 20$ kHz, $\gamma = 15$ Hz.
   
- $\gamma \ll \kappa , g$.  Ignore thermal noise
\begin{align}
    \dot a & = - \kappa a  + i g b^\da + \sqrt{ 2 \kappa } a\up{in},
    \\
    \dot b & = + i g a^\da .
\end{align}
- $\kappa \gg g$ => Adiabatically eliminate cavity mode
\begin{align}
    0 & = - \kappa a  + i g b^\da + \sqrt{ 2 \kappa } a\up{in},
    \\
    \dot b & = + i g a^\da .
\end{align}

## Solving mechanics
\begin{align}
    \label{eq:for_a}
    0 & = - \kappa a  + i g b^\da + \sqrt{ 2 \kappa } a\up{in},
    \\
    \label{eq:for_b}
    \dot b^\da & = - i g a.
\end{align}

- From~\eqref{eq:for_a}
\begin{equation}
    a = \frac{ i g }{ \kappa } b^\da + \sqrt{ \frac{ 2 }{ \kappa }} a\up{in}.
\end{equation}

- Therefore,
\begin{equation}
    \dot b^\da = - i g a = \frac{ g^2 }{ \kappa } b^\da - i \sqrt{\frac{ 2 g^2 }{ \kappa }} a\up{in}
\end{equation}

- Solution (define $G \equiv g^2/\kappa$)
\begin{equation}
    b^\da(t) = b^\da (0) e^{ G t }  - i \int_0^\tau \dd s e^{ G ( t - s ) } a\up{in} (s).
\end{equation}

## Solution for mechanics

\begin{multline}
    b^\da(t) = b^\da (0) e^{ G t }  - i \int_0^t \dd s e^{ G ( t - s ) } a\up{in} (s)
    \\
    = b^\da (0) e^{ G t } - i \sqrt{ e^{ 2 G t } - 1 } A\up{in},
\end{multline}
where
\begin{equation}
    A\up{in} = \sqrt{ \frac{ 2 G }{ 1 - e^{ - 2 G t }} } \int_0^t \dd s e^{ - G s  } a\up{in} (s).
\end{equation}

- This is a temporal mode of input light that is coupled to mechnaics.
- The input mode happens to be a superposition ($\int$) of the input modes
- commutation relation
\begin{equation}
    \comm{ A\up{in} }{ A\up{in}{}^\da } = 1.
\end{equation}

## Solving for output light

- Input-output relation
\begin{equation}
    a\up{out} = - a\up{in} + \sqrt{ 2 \kappa } a
\end{equation}

\begin{gather}
    a = \frac{ i g }{ \kappa } b^\da + \sqrt{ \frac 2 \kappa } a\up{in}
    \\
    a\up{out} = a \up{in} + i \sqrt{ 2 G } b^\da
    = a\up{in} + i \sqrt{ 2 G } 
    \left[  
    b^\da (0) e^{ G t }  - i \int_0^t \dd s e^{ G ( t - s ) } a\up{in} (s)
    \right]
\end{gather}

- Define output mode as
\begin{equation}
    A\up{out} = \sqrt{ \frac{ 2 G }{ e^{ 2 G t } - 1 }} \int_0^t \dd s e^{ G s  } a\up{out}.
\end{equation}

- Solution for the output mode
\begin{equation}
    A\up{out} = e^{ G  t }  A\up{in} + i b^\da (0) \sqrt{ e^{ 2 G t } - 1 }.
\end{equation}

## Conclusion and outlook

- Input-output transformations for field and mechanics
\begin{align}
    b^\da (t) & = \sqrt{ \mathcal G } b^\da (0) + i A\up{in} \sqrt{ \mathcal G - 1 },
    \\
    A\up{out} & = \sqrt{ \mathcal G } A\up{in} + i b^\da (0) \sqrt{ \mathcal G - 1 },
\end{align}
where $\mathcal G = e^{ 2 G t }$.

- Two-mode squeezing transformation, as if the Hamiltonian was
\begin{equation}
    H = A b + A^\da b^\da.
\end{equation}

- Input and output modes are defined 
\begin{equation}
    A\up{in} = \sqrt{ \frac{ 2 G }{ 1 - e^{ - 2 G t }} } \int_0^t \dd s e^{ - G s  } a\up{in} (s),
    \quad
    A\up{out} = \sqrt{ \frac{ 2 G }{ e^{ 2 G t } - 1 }} \int_0^t \dd s e^{ G s  } a\up{out}.
\end{equation}

# Summary 

## Electromechanics. Pulsed Entanglement

### Electromechanics

### Pulsed Entanglement
