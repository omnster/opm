\chapter[Optical parametric oscillators and amplifiers]{Optical parametric oscillators and amplifiers in resonators.
  Generation of squeezing}
\label{sec:l:parametric}

\begin{quote}
	In this lecture we talk about parametric processes, in particular, in cavities.  We observe different ways to analyze parametric squeezing of oscillations.
\end{quote}

\section{Parametric squeezing, semiclassical treatment} % <<<
\label{sec:parametric_squeezing_semiclassical_treatment}


In general, \idef{parametric systems} are such in which a \emph{parameter} can be changed in time.
A textbook example is swing.
To swing on a swing one has to periodically displace their center of gravity thus periodically changing the frequency of the oscillations.

Let's consider a swing (possibly, quantum) with frequency that follows a harmonic law: \( \Omega^2 (t) = \Omega^2 ( 1 + 2 \epsilon \cos \omega\s{d} ) \).%
\footnote{ In case of swing, its length changes as $l (t) = l_0 ( 1 + 2 \epsilon \cos \omega\s{d} t )$.
	As the frequency of swing is proportional to the square root of length,  $\Omega \propto l^{1/2}$, we obtain the law as in the text.}
At some point we will substitute $\omega\s{d} = 2 \Omega$.
The Hamiltonian of such system reads
\begin{equation}
	H = \frac{ p^2 }{ 2 \mu } + \frac 12 \mu \Omega^2 (t) x^2 =
	\frac{ p^2 }{ 2 \mu } + \frac 12 \mu \Omega^2 \left[ 1 + 2 \epsilon \cos \omega\s{d} t \right] x^2.
\end{equation}
Equations of motion for a system with this Hamiltonian are as follows
\begin{equation}
	\dot x = \frac 1 \mu p,
	\quad
	\dot p = - \mu \Omega^2 ( 1 + 2 \epsilon \cos \omega\s{d} t ).
\end{equation}

If the modulation of the parameter is weak (formally, $|\epsilon| \ll 1$), we can expect the dynamics of the system to be nearly harmonic.
That is, we can use an ansatz
\begin{equation}
	\label{eq:param:toslow}
	x = X \cos \Omega t + \frac{ 1 }{ \mu \Omega } P \sin \Omega t ,
	\quad
	p = P \cos \Omega t - \mu \Omega X \sin \Omega t ,
\end{equation}
where $X$ and $P$ are functions of time, however they change slowly on the timescale defined by $\Omega$.
More on this below.

Substituting the ansatz into the equations of motion, yields
\begin{align}
	\dot X \cos \Omega t + \frac{ 1 }{ \mu \Omega } \dot P \sin \Omega t & = 0,
	\\
	\dot P \cos \Omega t - \mu \Omega \dot X \sin \Omega t & =
	%
	- 2 \epsilon \mu \Omega^2 X \cos \omega\s{d} t \cos \Omega t
	- 2 \epsilon \Omega P \cos \omega\s{d} t \sin \Omega t.
\end{align}
Solving the system for $\dot X$ and $\dot P$ we obtain
\begin{align}
	\dot X & = 2 \epsilon \Omega X \cos \omega\s{d} t \cos \Omega t \sin \Omega t + 2 \epsilon \frac 1 \mu P \cos \omega\s{d} t \sin^2 \Omega t,
	\\
	\dot P & = - 2 \epsilon \mu \Omega^2 X \cos \omega\s{d} t \cos^2 \Omega t - 2 \epsilon \Omega P \cos \omega\s{d} t \sin \Omega t \cos \Omega t.
\end{align}
These equations are exact, no approximations made so far.

In order to simplify the equations, we use that $X$ and $P$ are \emph{slow} amplitudes\index{slow amplitudes}.
Formally, this means that we can average the equations over a period of harmonic motion
\begin{equation}
	\dot X (t) \approx \frac{ \Omega}{ 2 \pi } \int_{t }^{t + 2 \pi / \Omega } \dd{ t' } \dot X (t').
\end{equation}
Therefore,
\begin{equation}
	\dot X = \frac{ \epsilon }{ 2 \mu } P,
	\quad
	\dot P = \frac 12 \epsilon \mu \Omega^2 X,
	\quad
	\Longrightarrow
	\quad
	\ddot X = s^2 X, \text{ where } s = \frac{ \epsilon \Omega }{ 2 }.
\end{equation}
Note that these equations resemble very much the equations for an ordinary harmonic oscillator, multiplied by $\epsilon$.
However, the sign in $\dot P$ is opposite.
Finally, $s > 0$, so this is clearly not a harmonic oscillation.

The solution is
\begin{equation}
	\label{eq:sqz:unitary}
	X(t) = X(0) \cosh s t  - \frac{ 1 }{ \mu \Omega } P(0) \sinh s t ,
	\quad
	P(t) = P(0) \cosh s t  - \mu \Omega X(0) \sinh s t .
\end{equation}
One can see that the combinational variable
\begin{equation}
	\mu \Omega X(t) + P (t) = ( \mu \Omega X(0) + P(0) ) ( \cosh s t - \sinh s t ) \xrightarrow[t \to \infty]{} 0,
\end{equation}
so the state is becoming squeezed.
There are more robust ways to see this.

% >>>
\section{Quantum examination} % <<<
\label{sec:quantum_examination}

First, we notice that if $x,p$ are canonical variables of a harmonic oscillator, so are $X,P$: $\comm{X(t)}{P(t)} = \comm{x}{p} = i \hbar$.

Then, we determine the initial quantum state of the oscillator.
Let's assume for simplicity that it was in a thermal state, which means
\begin{equation}
	\vec r^{( xp  )} = 0,
	\quad
	\mmat V^{( xp  )} = ( 2 n_0 + 1 ) \diag( \frac{ \hbar }{ \mu \Omega } , \hbar \mu \Omega ).
\end{equation}
Using linear transformations~\eqref{eq:param:toslow} and rules of~\cref{sec:linear_gaussian_maps}, one can prove that in variables $X(0),P(0)$ the moments are the same.

To summarize, $X(0),P(0)$ are as well canonical quadratures of a harmonic oscillator in a thermal state.
Hence we can use the results of~\cref{sec:linear_gaussian_maps} to work out their statistics at later times.
We use the solution~\eqref{eq:sqz:unitary} to derive the covariance matrix
\( \mmat V^{(XP)} (t) \).
To investigate for squeezing we could find eigenvalues, however, first we transform it to dimensionless units.
In this units, $\mmat V^{(XP)} (0) = ( 2 n_0 + 1 ) \II_2$, and the eigenvalues of $\mmat V^{(XP)} (t)$ at later times read $e^{ \pm 2 s t }$.



% >>>
\section{Generation of squeezed light in cavity} % <<<
\label{sec:generation_of_squeezed_light_in_cavity}

\subsection{Nonlinearity} % <<<
\label{sec:nonlinearity}

In general case, the energy of electromagnetic field reads
\begin{equation}
	H = \int \dd v ( \vec E \vec D + \vec H \vec B ), \text{ where } \vec D = \vec D ( \vec E) = \epsilon \vec E + \chi | \vec E | \vec E + \dots
\end{equation}
The first term in the expansion recovers the standard Hailtonian $\vec E^2 + \vec H^2$.

The addition $\propto E^3$ causes nonlinear effects.
Unfortunately, the optical nonlinearities are weak, so the effect has to be enhanced by strong driving of the cavity at frequency $\omega\s{d}$ that creates a strong coherent component.

A strict treatment of this problem requires solving the Heisenberg-Langevin equations for the cavity operators.
While strict, this method does not focus on the parts that are relevant for the observation of the parametric effect.
Instead we will simply assume that the cavity is closed and inside it the field can be represented as a sum of a strong coherent part and a quantum fluctuating part.

% >>>
\subsection{Closed cavity} % <<<
\label{sec:closed_cavity}
We will eventually linearize the cavity field around this coherent component and search for the intracavity field in the form $a(t) \mapsto a(t) + \cE e^{ - i \omega\s{d} t }$.
Here $\cE$ is the strong classical component such that $\cE \gg a$.
Then the nonlinear term in the Hamiltonian can be recast in the form
\begin{multline}
	H\s{nonl} \propto \chi E^3 = \left( \cE e^{ - i \omega\s{d} t } + a + \hc  \right)^3
	=
	\left( \underbrace{ \left[  \cE e^{ - i \omega\s{d} t } + \cE^* e^{ i \omega\s{d} t }  \right] }_{ X } + \underbrace{ \left[ a + a^\da \right]}_{ x } \right)^3
	\\
	=
	X^3 + 3 X^2 x + 3 X x^2 + x^3 \approx X^3 + 3 X^2 x + 3 X x^2.
\end{multline}
The first term is a $c$-number, second describes a displacement in the phase space, so we focus on the third.

The total Hamiltonian $H = H\s{HO} + H\s{nonl}$ reads thus
\begin{equation}
	H = \hbar \Omega a^\da a + \hbar \frac{ \Gamma }{2 } \left[ \cE e^{ - i \omega\s{d} t } + \cE^* e^{ i \omega\s{d} t } \right] \left( a^\da + a \right)^2.
\end{equation}
In the frame rotating with free evolution, the Hamiltonian simplifies to
\begin{equation}
	H = \hbar \frac{ \Gamma }{2 } \left[ \cE e^{ - i \omega\s{d} t } + \cE^* e^{ i \omega\s{d} t } \right] \left( a^\da e^{ i \Omega t } + a e^{ - i \Omega t } \right)^2.
\end{equation}
After expanding this into a sum, each of the terms is multiplied by an exponential of the form
\begin{equation}
	e^{ \pm i ( \omega\s{d}  \pm k \Omega ) t }, \text{ where } k = 0,2.
\end{equation}
Each of these terms is \emph{fast} and would vanish under the RWA unless $k = 2$.
This means that, in order to see impact of the nonlinearity of the dynamics, the cavity has to be driven at twice the resonant frequency: $\omega\s{d} = 2 \Omega$.
In such case, the Hamiltonian reduces to
\begin{equation}
	H = \hbar \frac{ \Gamma }{2 } \left[  \cE a^\da{}^2 + \cE^* a^2 \right] \xrightarrow[\cE^* = \cE]{} \hbar \frac{ \Gamma }{2 } \cE ( a^\da{}^2 + a^2 ).
\end{equation}
The corresponding Heisenberg equations read
\begin{equation}
	\dot a = \frac i \hbar \comm{ H }{a } = i \frac{ \Gamma }{2 } \comm{ a^\da{}^2 }{ a  } = - i \Gamma a^\da,
	\qquad
	\dot a^\da = i \Gamma a
	\quad
	\Rightarrow
	\ddot a = \Gamma^2 a.
	\label{eq:cavity:squeezing:heiseq}
\end{equation}
This is an equation similar to the one we observed studying the swing.

\begin{exc}
	Solve~\cref{eq:cavity:squeezing:heiseq} for quadratures and prove the squeezing.
\end{exc}

% >>>

% >>>
\section{Parametric processes in cavity} % <<<
\label{sec:parametric_processes_in_cavity}

In the previous section we considered the perturbation to the Hamiltonian, formed by three-wave mixing $\delta H \propto \vec E \vec E \vec E$.
The effect of squeezing manifests itself when one of the places is taken by drive and the remaning two, by quantum fluctuations of one mode.
In this section we investigate the case when the quantum fluctuations of two modes come into play.

Let us consider the nonlinear cavity that supports two modes, $a$ and $b$, and is driven similarly to how it was in~\cref{sec:generation_of_squeezed_light_in_cavity}.
The Hamiltonian reads (we intentionally change notation $\Gamma \mapsto 2 \Gamma$):
\begin{multline}
	H = \hbar \Omega_a a^\da a + \hbar \Omega_b b^\da b + \hbar \Gamma ( \cE e^{ - i \omega\s{d} t } + \hc )( a + a^\da ) ( b + b^\da )
	\\
	\xrightarrow[\text{Rotating frame}]{} \hbar \Gamma ( \cE e^{ - i \omega\s{d} t } + \hc )( a e^{ - i \Omega_a t } + \hc ) ( b e^{ - i \Omega_b t } + \hc)
	\\
	= \sum \left[  ( \dots ) \times e^{ - i ( \omega\s{d} \pm  \Omega_a \pm \Omega_b ) t  }  \right] + \hc
	\label{eq:param:twomode}
\end{multline}
The remaining terms in the RWA are those giving zero in brackets.
There are two possibilities for this
\begin{equation}
	\omega\s{d} = \Omega_a + \Omega_b
	\qquad
	\text{ or }
	\qquad
	\omega\s{d} = \Omega_a - \Omega_b.
\end{equation}
where we assumed $\Omega_a > \Omega_b$.
Driving on the sum frequency enables the regime of parametric amplification, on the difference frequency, parametric conversion.
The two are considered below.

\subsection{Parametric amplification} % <<<
\label{sec:parametric_amplification}

Assuming driving on the sum frequency, we can keep in~\cref{eq:param:twomode} only terms (redefine $\Gamma \cE \mapsto \Gamma$)
\begin{equation}
	H = \hbar \Gamma ( a b + a^\da b^\da).
\end{equation}
Equations of motion in this regime read
\begin{equation}
	\dot a = - i \Gamma b^\da,
	\quad
	\dot b = - i \Gamma a^\da,
	\quad\Rightarrow\quad
	\ddot a = \Gamma^2 a.
\end{equation}

The solution reads
\begin{equation}
	a(t) = a(0) \cosh \Gamma t - i b^\da (0) \sinh \Gamma t,
	\quad
	b(t) = b(0) \cosh \Gamma t - i a^\da (0) \sinh \Gamma t.
\end{equation}

Or, similarly, in quadratures,
\begin{equation}
	X_a (t) = X_a (0) \cosh \Gamma t - P_b (0) \sinh \Gamma t,
	\quad
	P_a (t) = P_a (0) \cosh \Gamma t - X_b (0) \sinh \Gamma t,
\end{equation}
and the same for the other mode with replacement $a \leftrightarrow b$.

Taking into account $\cosh^2 x - \sinh^2 x = 1$, we define
\begin{equation}
	G = \cosh^2 \Gamma t,
\end{equation}
and rewrite
\begin{equation}
	\begin{pmatrix}
		X_a \\ P_a \\ X_b \\ P_b
	\end{pmatrix}
	(t) =
	\begin{pmatrix}
		\sqrt{ G } & 0 & 0 & - \sqrt{ G - 1 }
		\\
		0 & \sqrt{ G } & - \sqrt{ G - 1} & 0
		\\
		0 &  - \sqrt{ G - 1 } & \sqrt{ G } & 0
		\\
		- \sqrt{ G - 1 } & 0 & 0 & \sqrt{ G }
	\end{pmatrix}
	\begin{pmatrix}
		X_a \\ P_a \\ X_b \\ P_b
	\end{pmatrix}
	(0)
\end{equation}
This transformation can be used to study the covariance matrix dynamics.
Assuming the initial state to be vacuum, with CM $\II_4$ we can use~\cref{eq:gauss:map} to find the final CM of the form
\begin{equation}
	\mmat V\s{TMSV} =  ( 2 G + 1 ) \II_4 - 2 \sqrt{ G ( G - 1 )} \mathbb J_4, \text{ where } \mathbb J_4 = \antidiag[ 1,1 ,1,1].
\end{equation}

This state is known as \idef{Two-mode squeezed vacuum (TMSV)}.
This is an entangled bipartite state.
Individually each of the modes is in a thermal state with occupation $n\s{TMSV} = G$.

% >>>
\subsection{Parametric conversion} % <<<
\label{sec:parametric_conversion}

If the driving of parametric system is performed at the difference frequency, $\omega\s{d} = \Omega_a - \Omega_b$, the interaction takes the form
\begin{equation}
	H\s{PC} = \hbar \Gamma ( a b^\da + a^\da b ).
\end{equation}

The equations of motion

Solution

This is a passive interaction.
Defining the total excitation number operator $\oper{N} = a^\da a + b^\da b$, one can show that $\comm{\oper{N}}{H\s{PC}} = 0$, therefore, $\oper{N}$ remains unchanged under the dynamics driven by $H\s{PC}$.


% >>>

% >>>
\vfill
\section*{Excersises} % <<<
\label{sec:excersises:2}

\begin{exc}
	Prove that $X(t)$ and $P(t)$ defined in~\cref{sec:parametric_squeezing_semiclassical_treatment} are canonical quadratures of a harmonic oscillator.
\end{exc}

\begin{exc}
	Find statistical moments of $X(0)$, $P(0)$.
\end{exc}

\begin{exc}
	Analyze the relation between phase $\phi\s{d}$ caused by driving $\cE e^{ - i\omega\s{d} t - i \phi\s{d}}$, and the phase of squeezing of a quantum state.
\end{exc}

\begin{exc}
	Prove that $\comm{\oper{N}}{ H\s{PC}} = 0$.
\end{exc}

% >>>
