% \chapter{Pulsed quantum opto- and electromechanical quantum systems}

In previous lectures, we have acquainted ourselves with basic optomechanical Hamiltonians in the resolved sideband regime.
This lecture continues the theory assuming that resolved sideband condition holds, and we are driving the cavity on either upper or lower mechanical sideband.
This allows to arrange the local intracavity interaction to have either the type of beamsplitter, or two-mode squeezing.
In this lecture, we consider in more detail the effective transformation that takes place when the driving is pulsed.
Practically, that means that we consider the optomechanical interaction lasting a finite duration of time.

\section{Driving on the lower mechanical sideband} % <<<
\label{sec:driving_on_the_lower_mechanical_sideband}

When we drive the optomechanical cavity on the lower sideband, an excitation-hopping interaction is realized.
The linearized Hamiltonian of the optomechanical cavity reads (in the standard displaced rotating frame; as usual $\hbar = 1$)
\begin{equation}
  \label{eq:pulsed:ham:bs}
  H = \ii g ( \oa \ob\dg - \oa\dg \ob ),
\end{equation}
where we used some freedom in choosing the reference phase.
This results in the Hamiltonian that is anti-symmetric with respect to $\oa$ and $\ob$, and this is the price we pay for simpler equations of motion.
Below we drop the hats above the operators for brevity.

The Heisenberg-Langevin equations of motion read
\begin{subequations}
  \label{eq:hle:pulsed:red}
\begin{align}
  \label{eq:hle:pulsed:red:acav}
  \diff{a(t) }{t } & = \ii \comm{ H }{ a } - \kappa a(t) + \sqrt{ 2 \kappa } a\s{in} (t) = - g b(t) - \kappa a (t) + \sqrt{ 2 \kappa } a \s{in} (t),
  \\
  \diff{b(t) }{t } & = g a (t) - \frac \gamma 2 b (t) + \sqrt{ \gamma } b\s{th} (t).
\end{align}
\end{subequations}

Here we used
\begin{equation}
  \ii \comm{ H}{ a } = \ii \comm{ - \ii g a\dg b }{ a } = g b \comm{ a\dg }{ a } = - g b
\end{equation}
to write the first equation, and the fact that the Hamiltonian is anti-symmetric, to write the second line.
Moreover, $\kappa$ is the \emph{amplitude} decay rate of the cavity, and $\gamma$ is the \emph{energy} decay rate of the mechanical oscillator, or equivalently, viscous damping rate.
This is the traditional convention in optomechanics.
Operators $a\s{in}$ and $b\s{th}$ indicate quantum fluctuation inputs of the cavity and the mechanical modes, respectively.

The Eqs.~\eqref{eq:hle:pulsed:red} are linear first-order differential equations with stochastic terms, and these equations can be solved right away.
However, in the relevant for optomechanics regime of parameters, they can be simplified using certain approximations.
The solution based on these approximations can be helpful to understand some properties of the solution.

\subsection{Adiabatic elimination of the cavity mode} % <<<
\label{sec:adiabatic_elimination_of_the_cavity_mode}

In what follows, we will \emph{adiabatically eliminate} the cavity mode.
This is a standard approximation used in quantum optics, below we study the conditions necessary to apply it.
A recent theoretical publication~\cite{xie_commutation_2024} contains a substantial discussion of this technique with references.

First, let us reconsider the typical parameter regime of quantum optomechanics.
% Figure: Optomechanical rates % <<<
\begin{figure}[htb]
  \centering
  \begin{tikzpicture}[% <<<
    node distance = 3cm ,
    md/.style={draw, circle, minimum size = 1.5 cm},
    ]
    \node [md] (acav) {$a\s{cav}$} ;
    \node [md, right of = acav] (bmec) {$b\s{m}$} ;

    \node [md, right of = bmec ] (bth) {$b\s{th}$} ;

    \node [md, above left = 3mm and 2cm of acav ] (ain) {$a\s{in}$ } ;
    \node [md, below left = 3mm and 2cm of acav ] (aout) {$a\s{out}$ } ;


    \draw [->] (acav) to [bend left] (bmec) ;
    \draw [<-] (acav) to [bend right] (bmec) ;

    \node (g) at ($(acav)!.5!(bmec)$) {$g$} ;

    \draw [->] (ain) edge [bend left ]
      node [midway, above] {$\kappa$}
      (acav) ;
    \draw [<-] (aout) edge [bend right ]
      node [midway, above] {$\kappa$}
      (acav) ;

    \draw [<->, decorate,
      decoration={snake, pre length = 5pt, post length = 5pt}]
      (bmec) --
      node [above, midway ] {$\gamma/2$}
      (bth) ;

    \node [below = 15mm of g, draw , double, inner sep = 1em]
      {$\Delta = \omega\s{m} \gg \kappa \geq g \gg \gamma$ } ;

  \end{tikzpicture}% >>>
  \caption{Optomechanical interaction with relevant rates.}
  \label{fig:pulsed:red:pars}
\end{figure}% >>>
An illustration is given in~\cref{fig:pulsed:red:pars}.
Once we have applied the rotating wave approximation that leaves us with the Hamiltonian of~\cref{eq:pulsed:ham:bs}, the dominant rate in the system is the cavity linewidth $\kappa$.
The inverse cavity linewidth $\kappa^{-1}$ equals the time it takes the cavity mode to respond to the forces acting upon it.
The rate $\kappa$ exceeds all other rates in the system, correspondingly its inverse is smaller than other characteristic times.
Consequently, the intracavity field will react instantaneously to all the changes in the system, and remain approximately unpopulated.
Whatever excitations it gets, will be dissipated into its environment (the leaking field $a\s{out}$).
As a result, the time derivative of this mode can be approximately equated to zero.
In quantum optics, this is called \emph{adiabatic elimination} of the cavity mode $a\s{cav}$.

The second approximation we will use, is related to the thermal bath.
The coupling rate to the thermal mechanical environment, given by $\gamma$ is usually very small.
A numerical estimate can be obtained from the \Qf\ of the mechanical oscillator.
The \Qf\ equals $Q = \omega\s{m} / \gamma$ and can reach values as high as $Q \approx 10^5$ with clamped systems and $Q \gtrsim 10^{10}$ using levitated nanoparticles.
Given that normally $\omega\s{m}$ and $\kappa$ do not differ much, $\gamma \ll \kappa$.
Moreover, in practical realizations $g/\kappa \sim 10^{-1}$, therefore, $\gamma \ll g $.

As a result, here, we can ignore dissipation and decoherence of the mechanical oscillator.
The equations of motion, therefore, using these two approximations (adiabatic elimination of the cavity mode and omission of mechanical decoherence) are simplified to
\begin{subequations}
\begin{align}
  0 & = - g b - \kappa a + \sqrt{ 2 \kappa } a \s{in},
  \\
  \dot b & = g a.
\end{align}
\end{subequations}
We can immediately express $a$ from the first equation and substitute into the equation for $b$.
\begin{gather}
  \label{eq:pulsed:red:acav:algebr}
  a = - \frac{ g }{ \kappa } b + \sqrt{ \frac{ 2 }{ \kappa }} a\s{in},
  \\
  \label{eq:pulsed:red:mec}
  \dot b = g \left[ - \frac{ g }{ \kappa } b + \sqrt{ \frac{ 2 }{ \kappa }} a\s{in} \right]
  % \\
  = - \frac{ g^ 2 }{ \kappa } b + \sqrt{ 2 \frac{ g^2 }{ \kappa }} a\s{in }
  = - G b + \sqrt{ 2 G } a\s{in},
\end{gather}
where we denoted $G = \sfrac{ g^2 }{ \kappa }$.
This quantity has the units of frequency and has a physical meaning of the cooling rate of the mechanical mode.
Indeed, in absence of the input optical noise, we have $\dot b = - G b$, with a simple solution $b(t) = b(0) \ee^{ - G t }$.

\cref{eq:pulsed:red:mec} is exactly similar in the structure to the equation of motion for the field inside an open cavity:
\begin{equation}
  \dot a = - \kappa a + \sqrt{ 2 \kappa } a\s{in},
\end{equation}
which we have shown to have a solution in a form of an effective beamsplitter-like transformation.
Consequently,~\cref{eq:pulsed:red:mec} will have a similar solution (with appropriate replacements)
\begin{multline}
  \label{eq:pulsed:red:mecsol}
  b(t) = b(0) \ee^{ - G t } + \ee^{ - G t } \sqrt{ 2 G } \int_0^t \dl{s} \: e^{ G s } a\s{in} (s)
  \\
  =
  b(0) \sqrt{ \ee^{ - 2 G t }} + \Mode A\s{in} (t) \sqrt{ 1 - \ee^{ - 2 G t }}.
\end{multline}
Here we introduced an operator of the mode of the input field
\begin{equation}
  \Mode A \s{in} (t) = \sqrt{ \frac{ 2 G }{ e^{ 2 G t } -  1} } \int_0^t \dl{s} \: e^{ G s } a\s{in} (s),
  \quad
  \text{s.t. }
  \comm{ \Mode A\s{in} (t) }{ \Mode A\s{in} \dg (t) } = 1.
\end{equation}
We then use~\cref{eq:pulsed:red:acav:algebr,eq:pulsed:red:mecsol} and the input-output relation for the cavity field to write the solution for the leaking light $a\s{out}$.
The input-output relation reads
\begin{multline}
  a\s{out} (t) = - a\s{in} (t) + \sqrt{ 2 \kappa } a (t)
  =
  - a\s{in} (t) + \sqrt{ 2 \kappa } \left[ - \frac g \kappa b (t) + \sqrt{ \frac 2 \kappa } a\s{in} (t) \right]
  =
  a\s{in} (t) - \sqrt{ 2 G } b (t)
  \\
  = a\s{in} (t) - \sqrt{ 2 G }
  \left[b(0) \ee^{ - G t } + \ee^{ - G t } \sqrt{ 2 G } \int_0^t \dl{s} \: e^{ G s } a\s{in} (s)\right]
  \\
  = a\s{in} (t)
  - 2 G \ee^{ - G t } \int_0^t \dl{s} \: \ee^{ G s } a\s{in} (s)
  - \sqrt{ 2 G } b(0) \ee^{ - G t }.
\end{multline}
The operator $a\s{out} (t)$ describes an instantaneous value of the multimode leaking field.
We instead want to consider a single mode of this leaking light, described by
\begin{equation}
  \Mode A\s{out} (\tau) = \int_0^\tau \dl{t} \: a\s{out} (t) f\s{out} (t)
  %
  = \sqrt{ \frac{ 2 G }{ 1 - \ee^{ - 2 G \tau }}}
  \int_0^\tau \dl{t} \: a\s{out} (t) \ee^{ - G t }.
\end{equation}
The filtering function $f\s{out}(t)$ is chosen to maximize the contribution of $b(0)$ in $\Mode A\s{out}$.

By combining the two last equations, we get
\begin{multline}
  \sqrt{ \frac{ 1 - \ee^{ - 2 G \tau }}{ 2G }}\Mode A \s{out} (\tau )
  =
  \int_0^\tau \dl{t} \: a\s{in} (t ) \ee^{ - G t } - 
  2 G \int_0^\tau \dl{t} \: e^{ - 2 G t } \int_0^t \dl{s} \: \ee^{ G s } a\s{in} (s)
  -
  \sqrt{ 2 G } b(0) \int_0^\tau \dl{t} \ee^{ - 2 G t }.
\end{multline}
Consider the terms separately.
First,
\begin{equation}
    - \sqrt{ 2 G } b(0) \int_0^\tau \dl{t} \ee^{ - 2 G t }
    =
  \frac{ 1 }{ \sqrt{ 2 G }} \left( \ee^{ - 2 G \tau } - 1 \right) b (0).
\end{equation}
Then,
\begin{multline}
  \int_0^\tau \dl{t} \: a\s{in} (t ) \ee^{ - G t } - 
  2 G \int_0^\tau \dl{t} \: e^{ - 2 G t } \int_0^t \dl{s} \: \ee^{ G s } a\s{in} (s)
  \\
  =
  \int_0^\tau \dl{t} \: a\s{in} (t ) \ee^{ - G t } 
  - 2 G \int_0^\tau \dl{s} \: e^{ G s }  a\s{in} (s) \int_s^\tau \dl{t} \: \ee^{ - 2 G t }
  \\
  \int_0^\tau \dl{t} \: a\s{in} (t ) \ee^{ - G t } 
  - 2 G \int_0^\tau \dl{s} \: e^{ G s }  a\s{in} (s) \frac{ 1 }{ 2 G } \left[ \ee^{ - 2 G s } - \ee^{ - 2 G \tau } \right]
  % \\
  % \int_0^\tau \dl{t} \: a\s{in} (t ) \ee^{ - G t } 
  % - \int_0^\tau \dl{t } \: e^{ G t }  a\s{in} (t ) \left[ \ee^{ - 2 G t } - \ee^{ - 2 G \tau } \right]
  \\
  = \ee^{ - 2 G \tau } \int_0^\tau \dl{t} \: \ee^{ G t } a\s{in} (t)
  = \ee^{ - 2 G \tau } \Mode A\s{in} (\tau) \sqrt{ \frac{ \ee^{ 2 G \tau } - 1 }{ 2 G }}
  = \ee^{ - G \tau } \Mode A\s{in} (\tau) \sqrt{ \frac{ 1 - \ee^{ - 2 G \tau } }{ 2 G }}
\end{multline}

Collecting everything alltogether,
\begin{multline}
  \Mode A\s{out} (\tau ) = - \sqrt{ 1 - \ee^{ - 2 G \tau }} b(0) + 
  \ee^{ - G \tau } \Mode A\s{in} (\tau) \sqrt{ \frac{ 1 -  \ee^{ -  2 G \tau } }{ 2 G }} \sqrt{ \frac{ 2 G }{ 1 - \ee^{ - 2 G \tau }}}
  \\
  =
  - \sqrt{ 1 - \ee^{ - 2 G \tau }} b(0) + \sqrt{ \ee^{ - 2 G \tau }} \Mode A\s{in} (\tau).
\end{multline}

Denoting $\cT = \ee^{ - 2 G \tau }$, we can write the input-outptut relations describing the optomechanical interaction as follows
\begin{align}
  b(\tau) & = b(0) \sqrt{ \cT } + \Mode A\s{in} (\tau ) \sqrt{ 1 - \cT },
  \\
  \Mode A\s{out} (\tau) & = -  b(0) \sqrt{ 1 - \cT } + \Mode A\s{in} (\tau ) \sqrt{ \cT }.
\end{align}
Or in the vector form
\begin{equation}
  \binom {b(\tau)}{ \Mode A\s{out} (\tau) } =
  \begin{pmatrix}
    \sqrt{\cT} & \sqrt{ 1 - \cT }
    \\
    - \sqrt{ 1 - \cT } & \sqrt{\cT}
  \end{pmatrix}
  \binom{ b(0) }{ \Mode A\s{in} (\tau ) } =
  \begin{pmatrix}
    \cos \theta & \sin \theta 
    \\
    - \sin \theta & \cos \theta
  \end{pmatrix}
  \binom{ b(0) }{ \Mode A\s{in} (\tau ) },
\end{equation}
with $\theta = \arccos( \sqrt{ \cT })$.
Each of these forms suggests that the effective interaction between the light and the mechanics has the type of state swap (or, equivalently, beamsplitter or excitation-hopping).

It is important to again emphasize the meaning of the input and output modes described by the operators $\Mode A\s{in (out)}$.
Their definitions are as follows
\begin{align}
  \Mode A \s{in} (\tau) & = \sqrt{ \frac{ 2 G }{ e^{ 2 G \tau } -  1} } \int_0^\tau \dl{s} \: e^{ G s } a\s{in} (s),
  &
  \Mode A\s{out} (\tau)
  %
  & = \sqrt{ \frac{ 2 G }{ 1 - \ee^{ - 2 G \tau }}}
  \int_0^\tau \dl{s} \: a\s{out} (s) \ee^{ - G s}.
\end{align}
The definitions suggest that the mechanical oscillator couples naturally to the temporal modes of light that are filtered by an exponential envelope with the rate defined by the optomechanical cooling rate
\begin{equation}
  \Gamma\s{opt} = 2 G = \frac{ 2 g^2 }{ \kappa }.
\end{equation}
There are, in fact, different cooling rates: both $G$ and $\Gamma\s{opt}$ can be considered cooling rates, $G$ being an amplitude rate and $\Gamma\s{opt}$ being the energy rate.
We will focus on $\Gamma\s{opt}$ for simplicity.
This rate (approximately) defines the effective linewidth of the mechanical oscillator in presence of the red-detuned classical driving.
The fact that it is indeed a cooling rate can be seen from writing the expression for the mean occupation of the mechanical oscillator:
\begin{equation}
  \ev{ n\s{m} (t) } \equiv \ev{ b\dg (t) b(t) } = \ev{ b \dg (0) b(0) } \ee^{ - \Gamma\s{opt} t } = \ev{ n\s{m} (0) } \ee^{ - \Gamma\s{opt} t }.
\end{equation}
Indeed, the presence of a red-detuned drive enables cooling of the occupation of the mechanical oscillator.

In the expression above, following the approximations of this section, we ignore the thermal noise of the mechanical environment.
In reality, there will be a process that competes with the cooling, namely, a heating by the interaction with environment.


Furthermore, the optimal modes' definitions depend on the duration of the optomechanical interaction (above: $\tau$).



% >>>


% >>>
\section{Driving on the upper mechanical sideband} % <<<
\label{sec:driving_on_the_upper_mechanical_sideband}

When driving on the upper mechanical sideband, the effective interaction inside the cavity is \emph{two-mode squeezing} (or, equivalently, \emph{parametric amplification}).
Following the same procedure as above, we write the Heisenberg-Langevin equations of motion, adiabatically eliminate the cavity mode, and substitute the solutions into the input-output relations.
The result is given by
\begin{subequations}
  \label{eq:pulsed:blue:iofinal}
\begin{alignat}{3}
  \Mode A\s{out} (\tau) & = - \ee^{ G \tau } \Mode A\s{in} (\tau) - & \ii \sqrt{ \ee^{ 2 G \tau } - 1 } b (0)\dg,
  \\
  b(\tau) & = \ee^{ G \tau } b(0)  + & \ii \sqrt{  \ee^{ 2 G \tau } - 1 } \Mode A\s{in}\dg (\tau).
\end{alignat}
\end{subequations}
And the definition of the optimal modes of the light is changed:
\begin{align}
  \Mode A\s{in} (\tau)
  &
  = \sqrt{ \frac{ 2 G }{ 1 - \ee^{ - 2 G \tau }}}
  \int_0^\tau \dl{t} \: \ee^{ - G t } a\s{in} (t),
  &
  \Mode A\s{out} (\tau)
  &
  = \sqrt{ \frac{ 2 G }{ \ee^{ 2 G \tau } - 1 }}
  \int_0^\tau \dl{t} \: \ee^{ G t } a\s{out} (t).
\end{align}

\cref{eq:pulsed:blue:iofinal} describe a two-mode squeezing operation.

% >>>
