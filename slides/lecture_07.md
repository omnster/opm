---
title: Quantum Optomechanics. Lecture 7.
subtitle: Quantum precision limits.  Standard Quantum Limit.  Quantum Non-demolition interaction.
date: 2020-11-11
---


# Detecting mechanical displacement

## Heisenberg's microscope (a variation)

:::::::: {.columns}
:::: {.column width=49%}
\begin{figure}[htpb]
\begin{center}
    \begin{tikzpicture}[scale=1, transform shape,blockk/.style={minimum width = 7mm, minimum height = 3cm, fill = blue}]
    \node [ opacity = .4, blockk, very thick, draw = black] (fin) {} ;
    \node [draw = black, opacity = .2,  densely dashed, blockk, left=1cm of fin] (init) {} ;

        \draw [ transform canvas={yshift=-1em}, thick, -> ] (fin.south) -- node [midway, below] {$x(t)$} (init.south) ;

    \path (init.west) ++ (-4,1) coordinate (HDplace) ;
    \path (init.west) ++ (-4,-1) coordinate (input) ;

    \draw [<-] (init.west) -- node [midway, below, outer sep = 1em] {$a\up{in}$}   (input) ;
    \draw [<-, dashed] (fin.west) -- (input) ;

    \node [ draw, fill = gray, fill opacity=.1, minimum size  = 1cm , text opacity =1 ] (HD) at (HDplace) {HD} ;

    \draw [->] (init.west) -- node [midway, above, outer sep = 1em] {$a\up{out}$} (HD)  ;
    \draw [->, dashed] (fin.west) -- (HD)  ;


\end{tikzpicture}
\end{center}
\caption{Detecting displacement of a mechanical object.  HD --- Homodyne (phase-sensitive) detector.}%
\label{fig:heisenberg_microscope}
\end{figure}

Useful ineqs

- $a^2 + b^2 \geq 2 \sqrt{ a b }$
- $\Var x \Var p \geq \hbar^2 / 4$

::::
:::: {.column width=49%}

- Our purpose is to detect \emph{displacement}:  for $t_1, t_2$ measure $x(t_2) - x(t_1)$.
- Assume the mechanical object is free ($H = p^2 / 2 m$)
- $a\up{out} = a\up{in} e^{ 2 i k x(t)}$ $\mapsto$ detect $x$ via phase measurement

- detect $x(0)$ at $t = 0$ with imprecision $\Delta x(0)$
- imprecision of momentum $\Delta p(0)$: $\Delta x(0) \times \Delta p(0) \geq \hbar /2$
- after time 
\begin{equation}
    x(\tau) = x (0) + \frac{ \tau }{ m } p(0) + x\s{sig}
\end{equation}
-
\begin{equation}
    \Var x\s{sig} = \Var x(\tau) + \Var x (0) + \left( \frac{ \tau }{ m } \right)^2 \Var p (0)
\end{equation}
- detect $x(\tau)$ with arbitrary accuracy ($\Delta x(\tau) \to 0$)
\begin{equation}
    \Var x\s{sig} \geq \frac{ 2 \tau }{ m } \sqrt{ \Var x(0) \Var p(0) } = \frac{ \hbar \tau }{ m } .
\end{equation}
- This is called *Standard Quantum Limit (SQL)* of linear displacement detection
\begin{equation}
    x\s{SQL} = \frac{ \hbar \tau }{ m }.
\end{equation}


::::
::::::::

## Test mass is a harmonic oscillator

- make estimate of $x\s{sig}$ by subtracting 
\begin{equation}
    x(\tau = \frac{ 2 \pi / \Omega }{ 4 }) - x(0)
\end{equation}
- detection at $t = 0$ disturbs momentum 
\begin{equation}
    \Delta p(0) \geq \hbar/ ( 2 \Delta x(0))
\end{equation}
- this momentum kick propagates to subsequent measurement
\begin{equation}
    x (\tau) = x(0) \cos \Omega \tau + \frac{p(0)}{m \Omega} \sin \Omega \tau \Rightarrow \Var x \sim \frac{ \Var p(0)}{ ( m \Omega  )^2 }.
\end{equation}
- limit imprecision
\begin{equation}
    \Var x\s{sig} = \Var x(\tau) + \Var x (0) + \frac{ \Var p(0) }{ ( m \Omega  )^2 } \geq 2 \sqrt{ \Var x(0) \Var p(0) \frac{ 1 }{ ( m \Omega )^2 }} = \frac{ \hbar }{ m \Omega }.
\end{equation}


## Quantum apparatus (light) + Classical Harmonic oscillator (mechanics)

- measurement apparatus obeys Heisenberg Uncertainty:
\begin{equation}
    \Var x_L \Var p_L \geq \hbar / 4.
\end{equation}
- detected signal at $t = 0$:
\begin{equation}
    p\s{det} (0) = x_m (0) + \hat p_L (0),
\end{equation}
- perturbation of mechanical momentum (radiation pressure force)
\begin{equation}
    \Delta p_m (0) \sim \hat x_L (0)
\end{equation}
- after time
\begin{equation}
    \Var x\s{sig} \geq \Var p_L (0) + \Var p_L (\tau) +  \Var p_m (0)\frac{ 1 }{ ( m \Omega  )^2 } \geq \frac{ \hbar }{ m \Omega }.
\end{equation}

## Remarks on the origin of SQL

The basic reason in uncertainty  $\comm{A}{B} =  i C \Rightarrow \Delta A \Delta B \geq | C |^2/4$.

- we are attempting to detect $x(0)$ and $x(\tau)$ simultaneously
- these two variables do not commute and hence can't be measured with arbitrary precision simultaneously
\begin{equation}
    \comm{ x (0) }{ x (t) } \neq 0 \Rightarrow \Delta x(0) \Delta x(t) > 0.
\end{equation}
- Examples
    - for a free mass
    \begin{equation}
        \comm{ x (0) }{ x (\tau) } = \comm{ x (0) }{ x(0) + \frac{ p (0) \tau }{ m } + x\s{sig}} = i \hbar \frac{ \tau }{ m } \neq 0.
    \end{equation}
    - for an oscillator
    \begin{equation}
        \comm{ x (0) }{ x (\tau) } = \comm{ x (0) }{ x(0) \cos \Omega \tau  + p(0) \frac{ 1 }{ m \Omega } \sin \Omega \tau + x\s{sig}} = i \hbar \frac{ \sin \Omega \tau }{ m \Omega } \neq 0.
    \end{equation}
- solution: measure integrals of motion:
\begin{equation}
    \mathcal O:  \comm{ \mathcal O }{ H } = 0 \Rightarrow \dot{ \mathcal O } = 0 \Rightarrow \comm{ \mathcal O(t) }{ \mathcal O (t') } = 0.
\end{equation}

# Quantum non-demolition measurements

## Classical force detection

:::::::: {.columns}
:::: {.column width=49%}
- assume a classical force $f_0$ acting on a quantum harmonic oscillator
\begin{equation}
    H = \frac{ \Omega }{ 4 } ( p^2 + x^2 ) - \frac 12 f_0 x
\end{equation}
- equations of motion
\begin{align}
    \dot x & = \Omega p,
    \\
    \dot p & = - \Omega x + f_0 \cos \Omega t.
\end{align}
- solution in absence of the force
\begin{align}
    x (t) = X_c \cos \Omega t + X_s \sin \Omega t 
    \\
    p (t) = X_s \cos \Omega t - X_c \sin \Omega t
\end{align}
- $X_c$ and $X_s$ are *quadrature amplitudes*
\begin{align}
    X_c = x(t) \cos \Omega t - p(t) \sin \Omega t,
    \\
    X_s = x(t) \sin\Omega t + p(t) \cos \Omega t.
\end{align}
- commutations:
\begin{equation}
    \comm{ X_c }{ X_s } = \comm{ x(t)}{ p(t)} = 2 i.
\end{equation}
::::
:::: {.column width=49%}
- with force, full solution
\begin{align}
    x(t) & = X_c (0) \cos \Omega t + ( X_s(0)  + \tfrac 12 f_0 t ) \sin \Omega t , 
    \\
    p(t) & = ( X_s(0) + \tfrac 12 f_0 t ) \cos \Omega t  - ( X_c(0) - \frac{ f_0 }{ 2 \Omega } ) \sin \Omega t.
\end{align}
- if the force is weak (rather, not too strong): $\tfrac{ f_0 }{ 2 \Omega } \ll 1$, 
\begin{equation}
    X_c (t) = X_c (0),
    \quad
    X_s (t) = X_s (0) + \frac 12 f_0 t.
\end{equation}
- trying to come up with a strategy to detect $f_0$ having access to $x(t)$ via a phase measurement.
    - couple to $X_s (t)$
    - that is, measure
    \begin{equation}
        \mathcal I = \int_0^\tau x(t) \sin \Omega t \dd t.
    \end{equation}
- long integration time $\tau = 2 \pi n / \Omega, \quad n \in \mathbb Z$,
\begin{equation}
    \mathcal I = \frac{ p_0 t }{ 2 } + \frac{ f_0 t^2 }{ 8 }.
\end{equation}

::::
::::::::

## Why does it work

:::::::: {.columns}
:::: {.column width=49%}
- Equations of motion for quadrature amplitudes
- without force
\begin{align}
    \dot X_c (t)  & = 0
    \\
    \dot X_s (t)  & = 0.
\end{align}
- $X_{c,s}$ are integrals of motion,
\begin{equation}
    \comm{ X_i (t) }{ X_i (t') } = 0.
\end{equation}
- with force
\begin{align}
    \dot X_c (t)  & \propto f_0 \sin 2 \Omega t
    \\
    \dot X_s (t)  & \propto f_0.
\end{align}
- commutation relation preserves since $f_0$ is classical
- in RWA $\sin 2 \Omega t = 0$.
- validity of RWA = slow amplitudes = weak force.

::::
::::::::

## QND in optomechanics

:::::::: {.columns}
:::: {.column width=49%}

- Hamiltonian of optomechanical system with classical mechanical force
\begin{equation}
    H = - \Delta a_L^\da a_L + \omega b_m^\da b_m - g X_L X_m - f_0 X_m \cos \omega t.
\end{equation}
- light couples to the instantaneous position $x_m (t)$
\begin{equation}
    \dot Y_L \propto x_m (t) \sim x^m_c \cos \omega t + ...
\end{equation}
- Want to couple to quadrature amplitudes
\begin{equation}
    \dot Y_L \propto x^m_c.
\end{equation}
- the bad guys are free evolution terms, need to get rid of them
\begin{equation}
    H_0 =- \Delta a_L^\da a_L + \omega b_m^\da b_m  .
\end{equation}
- set $\Delta = 0$ and go to rotating frame defined by $H_0$\
(ignore force):
\begin{equation}
    H = - g ( x^L_c \cos \Delta t - x^L_s \sin \Delta t )( x^m_c \cos \omega t + x^m_s \sin \omega t ).
\end{equation}
we want a coupling $x^L_c x^m_c$ or alike

::::
:::: {.column width=49%}
- solution: modulate $g \mapsto g \cos ( \omega t + \phi\s{mod} )$
\begin{multline}
    H = - g \cos \omega t \ x^L_c ( x^m_c \cos \omega t + x^m_s \sin \omega t )
    \\
    = - \frac{ g }{ 2 } \Big[ x^L_c x^m_c ( 1 + \cos 2 \omega t ) + x^L_c x^m_s \sin 2 \omega t \Big].
\end{multline}
assuming RWA ($g \ll \omega$)
\begin{equation}
    H = - \frac g 2 x^L_c x^m_c.
\end{equation}

- This is a quantum non-demolition coupling
- equations of motion:
\begin{align}
    \dot x^m_c & = 0 , & \dot x^m_s & = g x^L_c,
    \\
    \dot x^L_s & = g x^m_c.
\end{align}
- if we arrange action of a force so that it displaces $x^m_c$, we can repeatedly detect $x^m_c$ while absorbing back-action in $x^m_s$.
- or rather we adjust $\phi\s{mod}$ to couple to a proper quadrature amplitude of mechanics
::::
::::::::

## QND in optomechanics (Recap)

The Hamiltonian (the full Hamiltonian)
\begin{equation}
    H = g X_L X_m +  ...  f\s{cl} (t).
\end{equation}
where $X_i$ are the *slow amplitudes*/quadrature amplitudes.

# Other ways to beat SQL (hand-wavy)

## General scheme of linear measurements (hand-wavy)

\begin{figure}[htpb]
\begin{center}
    \begin{tikzpicture}[scale=1, transform shape,blck/.style={minimum width = 2cm, minimum height = 2cm}]

        \node [blck, fill = red!20] (probe) {Probe} ;

        \node [blck, fill = blue!20, text width = 2cm, right = 3cm of probe, align = center] (meas) {Measurement Apparatus} ;

        \node [minimum width = 1cm, minimum height = 1cm , right = 3cm of meas, draw ] (det) {Det} ;

        \node [ below = 1em of probe ] {$(x,p)$} ;
        \node [below =  1em of meas ] {$(x_L, p_L)$} ;

        \draw [<-] (probe.west) -- node [above, midway] {$F$} ++ (-15mm, 0) ;

        \draw [ transform canvas = {yshift= 1em} , -> ] (probe) -- node [above, midway] {$x$} (meas)  ;
        \draw [ transform canvas = {yshift=-1em} , -> ] (meas) -- node [below, midway] {$F\s{BA}$} (probe)  ;

        \draw [->] (meas.east) -- node [above, midway] {$x_L$} (det) ;

        \draw [->] (det) -- node [above, midway] {$\bar x = x_L + x\s{fl}$} ++ (3cm,0) ;


\end{tikzpicture}
\end{center}
\caption{Generalized sketch of linear measurements}%
\label{fig:}
\end{figure}

:::::::: {.columns}
:::: {.column width=49%}
- SQL arises when we attempt to measure linear combinations of non-commuting observables
- In the scheme, mixtures occur either via $F\s{BA}$ (conjugate to $x_L$) being mapped onto $x$ that is subsequently transmitted to $x_L$

::::
:::: {.column width=49%}
::::
::::::::

