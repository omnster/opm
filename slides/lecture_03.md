---
author: Andrey Rakhubovsky
date: 2020-10-14
title: Quantum Optomechanics. Lecture 3.
subtitle: Parametric Processes (cont.),  Gaussian states and operations, Mechanical oscillators.
fontsize: 10pt
fontfamily: libertine
header-includes: | 
 \usepackage{tikz}
 \usetikzlibrary{positioning}
 \usetikzlibrary{arrows}
---

# Introductory Quantum Optics

## Parametric Processes

### Three-wave mixing (Degenerate frequencies)

Hamiltonian has a term $\propto \text{(Electric Field)}^3$.

The field has a strong coherent part $E$ and weak quantum fluctuations $e = a + a^\da$.

$$H = \hbar \Omega_a a^\da a + \hbar \tfrac \Gamma 2 ( E e^{ - i \omega\s{d}  t} + \hc) ( a^\da + a )^2$$

---

#### Hamiltonian in RWA

In rotating frame ($H\s{rf} = \hbar \Omega_a a^\da a$)

\begin{multline}
    H = \hbar \tfrac \Gamma 2 ( E^{ - i \omega\s{d} t } + \hc )( a^\da e^{ i \Omega_a t } + \hc )^2
    \\
    \Rightarrow \sum [\dots] \exp[ - i ( \omega\s{d} \pm \Omega_a \pm \Omega_a ) t ].
\end{multline}

Given $\omega_{d} = 2 \Omega_a$ we arrive (in RWA) to the *single-mode squeezing Hamiltonian*

$$H = \hbar \frac{ \Gamma E }{ 2 } ( (a^\da)^2 + a^2 ).$$

---

#### Equations of motion

$$\dot a = - i \Gamma a^\da,
\quad
\dot a^\da = i \Gamma a.$$

$$\Rightarrow a(t) = a(0) \cosh ( \Gamma t ) - i a^\da (0) \sinh ( \Gamma t ).$$

. . .

$$X(t) = a(t) + a^\da (t) = X(0) \cosh ( \Gamma t ) - P(0) \sinh ( \Gamma t ),$$

$$P(t) = ( a(t) - a^\da (t))/i = P(0) \cosh (\Gamma t ) - X (0) \sinh (\Gamma t )$$

### Gaussian states

* A GS is a state with Gaussian Wigner function  (or $P$-function, or $Q$-function, ... ).

* Vacuum, thermal, coherent, squeezed states are Gaussian

* A $N$-mode system is described by vector of operators $r = ( X_1, P_1, \dots, X_N , P_N)$.

* In general case, density matrix $\rho$ contains all expectations $r, r^2, r^3, \dots$

* Gaussian states can be fully described by the first two moments
    
    * vector of means $\mu$
    * covariance matrix $V$

### Covariance matrix

#### Definition 

If $\avg{A} \equiv \Tr [ \rho A ]$, then

$$\mu_i = \avg{ r_i } $$

$$V_{ij } = \sym{ r_i - \mu_i }{ r_j - \mu_j } = \sym{ r_i }{ r_j } - \mu_i \mu_j.  $$

Here $\circ$ means symmetric product

$$ A \circ B = \frac 12 \Big[  A B + B A \Big] $$

Symmetric product is linear in each argument


---

Exercise: using $x = a + a^\da ,  p = i ( a^\da - a )$, compute $\mel{0}{xp}{0}$.


### Single-mode squeezing

Transformations ($c = \cosh ( \Gamma t ) , s = \sinh (\Gamma t )$)

$$ X(t) = X_0 c - P_0 s, \quad P(t) = P_0 c - X_0 s. $$

Averaging is linear: $\Tr [  \rho \sum_i c_i x_i ] = \sum_i c_i  \Tr [ \rho x_i ]$.

The transformation maps zero-mean to zero-mean


### Covariance matrix

$$V =
\begin{pmatrix}
    \avg{ X(t)^2 } & \avg{ X(t) P(t) + P(t) X(t) }/2
    \\
    \avg{ X(t) P(t) + P(t) X(t) }/2 & \avg{ P(t)^2 }
\end{pmatrix}.$$

. . .

$$ X(t) = X_0 c - P_0 s, \quad P(t) = P_0 c - X_0 s. $$

\begin{multline}
    \avg{ X(t)^2 } = \avg{ ( X_0 c - P_0 s )( X_0 c - P_0 s ) }
    \\
    = \avg{ X_0^2 } c^2 + \avg{ P_0^2 } s^2 - c s \avg{ X_0 P_0 + P_0 X_0 }.
\end{multline}

### Gaussian processes

If a process maps quadratures 

$$r \mapsto r' = T r,$$

then

$$\mu \mapsto \mu' = T\mu$$

and

$$V_{ij } \mapsto V_{ij }' = \sym{ T_{ik } r_k }{ T_{jl } r_l } = T_{ik } \sym{ r_k }{ r_l } T\up{T}_{lj } = ( T V T\up{T} )_{ij }.
$$

---

Exercise: find out transformation $T$ for single-mode squeezing and compute the covariance matrix.

Prove squeezing by finding eigenvalues of the covariance matrix.


### Three-wave mixing (Non-degenerate frequencies)

Nonlinearity combines three field vectors. 

Single-mode squeezing takes one strong pump $E$ and two weak signals $a,a$

Assume one pump and two signal modes: $a$ and $b$.


### Hamiltonian

$$H = \hbar \Omega_a a^\da a +\hbar \Omega_b b^\da b + \hbar \Gamma ( E e^{ - i \omega\s{d} t } + \hc )( a + a^\da )( b + b^\da )$$

$$\Rightarrow\s{RF} \hbar \Gamma (E e^{ - i \omega\s{d} t } + \dots )( a e^{ - i \Omega_a t } + \dots)( b e^{ - i \Omega_b t } + \dots )$$

$$\Rightarrow \sum [ \dots ] \exp[  - i ( \omega\s{d} \pm \Omega_a \pm \Omega_b ) t ].$$

. . .

Survive only terms $\omega\s{d} = \Omega_a + \Omega_b$ or $\omega\s{d} = \Omega_a - \Omega_b$.


### Difference-frequency pump $\omega\s{d} = \Omega_a - \Omega_b$

$$H =  \hbar \Gamma (E e^{ - i \omega\s{d} t } + \dots )( a e^{ - i \Omega_a t } + \dots)( b e^{ - i \Omega_b t } + \dots )$$

In RWA survive the terms $\propto \omega\s{d} - \Omega_a + \Omega_b = 0$.

. . .

$$H = \hbar \Gamma E ( a^\da b + a b^\da) $$

. . .

Known as Parametric conversion:  $a^\da b$ annihilates a photon in $b$ and creates in $a$.

Also the Hamiltonian of Beam-splitter interaction, or photon-hopping interaction.

$$U\s{Beam-Spl.} =  e^{ - i \theta (a b^\da + a^\da b ) }.$$


### Sum-frequency pump $\omega\s{d} = \Omega_a + \Omega_b$

$$H =  \hbar \Gamma (E e^{ - i \omega\s{d} t } + \dots )( a e^{ - i \Omega_a t } + \dots)( b e^{ - i \Omega_b t } + \dots )$$

In RWA survive the terms $\propto \omega\s{d} - \Omega_a - \Omega_b = 0$.

. . .

$$H = \hbar \Gamma E ( a^\da b^\da + a b) $$

. . .

Known as 

- parametric amplification 
- two-mode squeezing
- parametric down-conversion
- spontaneous parametric down-conversion (SPDC)

### SPDC (spontaneous parametric down-conversion)

$$H\s{SPDC} \propto \lambda ( a^\da b^\da + a b ) $$

Assume $\lambda \ll 1$

$$e^{ - i H\s{SPDC} } \ket{ 00 } \sim \left[ 1 - i H\s{SPDC} \right] \ket{ 00 }$$
$$\sim \left[ 1 + \lambda( a^\da b^\da + a b ) \right] \ket{ 00 } = \ket{00} + \lambda\ket{11}$$

$$\bra{1}_a e^{ - i H\s{SPDC} } \ket{ 00 } \approx \ket{1}_b.$$

### Gaussian operations (summary)

Processes that map GS to GS are described by Hamiltonians up to quadratic in bosonic operators

- displacements $\xi a^\da + \xi^* a$:   coherent driving of cavity (application of classical force)
- phase rotations $\Omega a^\da a$: eigen evolution of harmonic oscillator
- passive mixing $\theta a^\da b + \theta^* a b^\da$: beam-splitters
- single-mode squeezing $\zeta a^\da {}^2 + \zeta^* a^2$: parametric driving (nonlinear crystal/optomechanical system)
- two-mode squeezing $\zeta a^\da b^\da + \zeta^* a b$: parametric driving

---

Parametric driving requires performing work upon a system in a clever fashion.

Swinging on a swing: periodic change of length of a swing (precisely timed).

Nonlinear crystal driving requires frequency and phase tuning.


---

Exercise:

Derive the maps $T$ for the remaining Gaussian operators

Be careful with the single-mode displacement

###  Brief summary of needed quantum optics

This far we have established conventions about

- harmonic oscillator
- optical cavity
- Heisenberg-Langevin equations
- input-output equations
- Gaussian operations
    

# Mechanical oscillators

### Definitions 

- point-like particle does not reflect much
- prefer brick on a string
- if the string is sufficiently long, behaves as a pointlike particle

### Example

![Quadruple pendulum suspension developed by the aLIGO UK team. Left: Pendulum with its main structural components Right: detail of the magnets and main chain/reaction chain. Figure adapted from Aston et al (2012).](./fig/glasgow_suspend.png "Suspensions")

---

![Optomechanical platforms](./fig/Figure_Overview_OptomechanicsFinal.png "Platforms")


### Formal treatment

- There is a displacement field $\vec u (\vec r , t )$ 
- can be decomposed to eigenmodes $\sum \vec u_i (\vec r ) x_i (t)$
- typically there are very few modes of interest (usually, one)

$x_{(i)}$ is then the mode of interest, a HO itself.

### Example: Membrane oscillations

https://en.wikipedia.org/wiki/Vibrations_of_a_circular_membrane

![Vibrational modes of a circle membrane (from wikipedia)](./fig/membrane_modes.png "Membrane modes")

### Susceptibility of mechanical oscillator

Equation of motion

$$m\s{eff} \ddot x + h \dot x + k x = \bar F\s{ext} $$

$$\Rightarrow \ddot x + \gamma \dot x + \omega_0^2 x = F\s{ext}$$

In spectral domain $x(\Omega) = F\s{ext} (\Omega) \chi (\Omega)$, $\chi$ is *susceptibility*

$$\chi(\Omega) = \frac{ 1 }{  ( \omega_0^2 - \Omega^2 ) - i \gamma \Omega }$$

- $\omega_0$ defines peak susceptibility
- $Q = \omega_0/\gamma$ is the $Q$-factor, defines the resonance width
- typical $Q$s of mechanics lie in range $Q \sim 10^{6+}$,  $Q = 10^8$ are available.

### Examples

![Susceptibility as a function of driving frequency](./fig/suscept.pdf "Susceptibility")


### Heisenberg-Langevin equation 

As opposed to cavities where
$$\dot a = - i \Omega a - \kappa a + \sqrt{ 2 \kappa } a\up{in}$$

Mechanical damping force enters the equation of motion for momentum
$$\dot p = - \Omega x - \gamma p + \sqrt{ 2 \gamma } p\up{in},$$
$$\dot x = \Omega p.$$

The source of $p\up{in}$ is the coupling to the surrounding mechanical modes.

This noise is markovian and thermal:
$$\sym{p\up{in}(t)}{p\up{in}(t')} = ( 2 n\s{th} + 1 ) \delta ( t - t' ).$$

---

Exercise:  From solution for $x(t), p(t)$, derive the properties of $p\up{in}$ required to preserve canonical commutation $\comm{x(t)}{p(t)}$.

### Quantum states (mostly thermal)

In equilibrium mechanics relaxes to the thermal state specified by the temperature  $\Theta$
\begin{equation}
  \rho\s{th} = \left( 1 - e^{ - \dfrac{ \hbar \Omega}{ k\s{B} \Theta}} \right) \sum_{n = 0}^{\infty} e^{ - \dfrac{ n \hbar \Omega }{ k\s{B} \Theta}} \dyad n n ,
\end{equation}

The mean occupation
$$\avg n = \left[  1 - e^{ - \frac{ \hbar \Omega }{ k\s{B} \Theta }} \right]^{-1} \approx \frac{ k\s{B} \Theta }{ \hbar \Omega } \approx 20 \frac{ \Theta [\text{mK}]}{ \Omega [\text{MHz}] }.
$$

That is at the temperatures of dilution refrigerator ($\sim 10$ mK) a typical membrane ($\sim 10$ MHz) will have mean occupation about 20 phonons.

### Reflection of light (finally, optomechanics!)

Assume a stream of photons reflects from a mechanical oscillator.

![Elastic photon scattering](./tikz/photons_reflect.pdf "Photon scattering")

- photon momentum $p = \hbar k = \hbar \frac{ \omega\s{L}} c$
- transferred momentum $\Delta p = 2 p = \hbar \frac{ 2 \omega\s{L} } c$.
- momentum spread of mechanics in vacuum $p\s{zpf} = \sqrt{ \hbar \mu \Omega }$.

--- 

![Phase space displacement from a single-photon reflection](./tikz/phase_space_displacement.pdf "Phase space")

To see the effect we need  $\Delta p > p\s{zpf}$

---

To see the effect we need  $\Delta p > p\s{zpf}$

$$ \hbar \frac{ 2 \pi }{ \lambda\s{L}} \approx 10^{-28}  >  \sqrt{ \hbar \mu \Omega } \approx 10^{-23}$$

Using typical parameters 

- $\lambda\s{L} = 1.064 \times 10^{-6}$ nm
- $\Omega = 10^6$ Hz
- $\mu = 10^{-18}$ kg

Need to enhance the interaction.

### Cavity optomechanics

- Put everything into cavity
- Make photons reflect multiple times

