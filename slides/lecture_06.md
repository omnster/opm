---
title: Quantum Optomechanics. Lecture 6.
subtitle: Pulsed Quantum State exchange.  Opto-electro-mechanical transducers.
date: 2020-11-11
---

# Recapitulation

## Electromechanical interaction in Blue detuning

- In blue detuning the steady state does not exist => use pulsed driving
- After proper linearization, the interaction (where $g = g_0 \sqrt{ \bar n\s{mw}}$)
\begin{equation}
    H = g ( a^\da b ^\da + a b ),
\end{equation}
- Write Langevin equations for $a,b$, make suitable approximations
    - ignore thermal noise
    - adiabatically eliminate cavity mode
    - 
\begin{align}
    0 & = - \kappa a + i g b^\da + \sqrt{ 2 \kappa } a\up{in},
    \\
    \dot b & = + i g a^\da .
\end{align}
- Solution gives input-output relations (next slide)

## Input-output for blue detuned pulsed interaction

\begin{align}
    b\up{out} = \sqrt{ \mathcal G } b\up{in} + i \sqrt{ \mathcal G - 1 } A\up{in}{}^\da,
    \\
    A\up{out} = \sqrt{ \mathcal G } A\up{in} + i \sqrt{ \mathcal G - 1 } b\up{in}{}^\da,
\end{align}
where 
\begin{equation}
    \mathcal G = e^{ 2 g^2 \tau  / \kappa },
\end{equation}

and the input-output modes are defined as ( $\comm{A^k }{ A^k{}^\da} = 1$ )
\begin{equation}
    A\up{in} = \sqrt{ \frac{ 2 G }{ 1 - e^{ - 2 G t }} } \int_0^t \dd s e^{ - G s  } a\up{in} (s),
    \quad
    A\up{out} = \sqrt{ \frac{ 2 G }{ e^{ 2 G t } - 1 }} \int_0^t \dd s e^{ G s  } a\up{out}.
\end{equation}

- recall,
\begin{equation}
    \dot a = - \kappa a + \sqrt{ 2 \kappa } a\up{in},
    \quad
    \comm{a(t)}{a^\da(t)} = 1,
    \quad
    \comm{a\up{in} (t) }{ a\up{in}{}^\da (t') } = \delta ( t - t') 
\end{equation}

# Red-detuned interaction. State swap

## General Picture

\begin{equation}
    \frac 1 \hbar H\s{lin} = \Omega\s{cav} a^\da a + \omega\s m b^\da b + g (a^\da +  a) ( b^\da + b ) .
\end{equation}

- Drive on lower mechanical sideband 
- $\omega\s{drive} = \Omega\s{cav} - \omega\s m$, drive at difference frequency, therefore, parametric conversion
- Drive photons (at $\omega\s d$) combine with mechanical phonons (at $\omega\s m$) to produce resonant cavity photons (at $\Omega\s{cav} = \omega\s d + \omega\s m$).

\centering
\begin{tikzpicture}
    \draw [ draw = red , -> ] (0,0) -- node [above, midway, red] {$\omega\s d$}  ++ (4,0) ;
    \draw [ draw = magenta, -> ] (4.1,0) -- node [above, midway, magenta] {$\omega\s m$} ++ (.9,0) ;
    \draw [ draw = blue, -> ] (0,-2mm) -- node [ below, midway , blue ] {$\Omega\s{cav}$} ++ (5,0) ;
\end{tikzpicture}


## In RWA
\begin{equation}
    \frac 1 \hbar H\s{lin} = g ( a^\da\s c b\s m + a\s c b^\da\s m ).
\end{equation}

Requirements
- pumping at lower mechanical sideband
- resolved sideband regime ($\omega\s m \gg \kappa, g , \gamma\s m$)

\begin{figure}[h]
\begin{tikzpicture}
    [
    axes/.style={draw},
    ]
    \draw [axes, -> ] (-7,0) -- node [at end, above] {$\omega$} ++ (9.4,0) ;
    \draw [blue] (0,-0.2) -- node [ at start, below ] {$\Omega\s{cav}$} node [at end, above] {$a\s c$} ++ (0,3) ;
    \draw [red] (-2.5 , -0.2) -- node [at start, below ] {$\omega\s d$} node [at end, above] {$\alpha$} ++ (0,3) ;
    \draw [magenta] (-6.5,-0.2) -- node [at start, below ] {$\omega\s m$} node [at end, above] {$b\s m$} ++ (0,3) ;
    % \draw plot [domain = 6:8] ({\x},{4/(1 + (\x-7)^2/4)} ;
    \draw [draw, samples = 200] plot[domain=-6:2] ({{\x},{2.5/(1 + (\x/0.3)^2}}) ;
    \draw [<->] (-0.3,1.25) -- node [at end, right] {$2 \kappa$} ++ (0.6,0) ;

    \draw [<->] (-2.5,0.6) -- node [near start, above] {$\omega\s m$} ++ (2.5,0) ;

\end{tikzpicture}
\caption{Pumping on the lower mechanical sideband in the resolved sideband regime.}
\end{figure}

## Equations of motion

\begin{align}
    \dot a & = - \kappa a  - i g b + \sqrt{ 2 \kappa } a\up{in},
    \\
    \dot b & = - \frac \gamma 2 b - i g a + \sqrt{ \gamma } b\up{th}.
\end{align}

. . .

Numerical parameters:\
$\omega\s m = 10$ MHz, $\kappa = 300$ kHz, $g = 30$ kHz, $\gamma\s m \sim 50$ Hz.

Important approximations

- no thermal noise
- adiabatic elimination of cavity mode
\begin{align}
    0 & = - \kappa a  - i g b + \sqrt{ 2 \kappa } a\up{in},
    \\
    \dot b & =- i g a.
\end{align}

## Solution of Langevin equations

\begin{align}
    0 & = - \kappa a  - i g b + \sqrt{ 2 \kappa } a\up{in},
    \\
    \dot b & =- i g a.
\end{align}

. . .

\begin{equation}
    a = \frac{ - i g }{ \kappa } b  + \sqrt{ \frac 2 \kappa } a\up{in},
\end{equation}

. . .

Substitute for $\dot b$ ($G = g^2 / \kappa$)
\begin{equation}
    \dot b = - \frac{ g^2 }{ \kappa } b  - i \sqrt{ \frac{ 2 g^2 }{ \kappa } } a\up{in} = - G b - i \sqrt{ 2G } a\up{in}.
\end{equation}

. . .

\begin{equation}
    b (t) = e^{ - G t  } b (0) - i  \sqrt{ 2 G } \int_0 ^t \dd s e^{ - G ( t - s ) } a\up{in} (s)
\end{equation}

## Temporal modes

\begin{equation}
    b (t) = e^{ - G t  } b (0) - i  \sqrt{ 2 G } e^{ - G t } \int_0 ^t \dd s e^{ G s } a\up{in} (s).
\end{equation}

. . .

Define input mode: $A\up{in} = C\up{in} \int_0^t \dd s e^{ G s } a\up{in}.$

. . .

\begin{multline}
    \comm{ A\up{in}}{ A\up{in}{}^\da} = | C\up{in} |^2 \iint_0^t \dd s \dd{ s'} e^{ G ( s + s') } 
    \underbrace{ \comm{ a\up{in} (s)}{a\up{in}{}^\da (s')}}_{\delta ( t - t') }
    \\
    = | C\up{in} |^2 \int_0^t \dd s e^{ 2 G s } = \frac{ | C\up{in} |^2 }{ 2 G } ( e^{ 2 G t } - 1 ) = 1.
\end{multline}

. . .

Up to a phase factor
\begin{equation}
    A\up{in} = \sqrt{ \frac{ 2 G }{ e^{ 2 G t  } -1  } } \int_0^t \dd s e^{ G s } a\up{in} (s).
\end{equation}

***

\begin{equation}
    A\up{in} = \sqrt{ \frac{ 2 G }{ e^{ 2 G t  } -1  } } \int_0^t \dd s e^{ G s } a\up{in}.
\end{equation}

\begin{multline}
    b (t) = e^{ - G t  } b (0) - i \sqrt{ 2 G } e^{ - G t } \int_0 ^t \dd s e^{ G s } a\up{in} (s)
    \\
    = e^{ - G t } b (0) - i e^{ - G t } \sqrt{ e^{ 2 G t } - 1 } \sqrt{ \frac{ 2 G }{ e^{ 2 G t } - 1 }}
    \int_0 ^t \dd s e^{ G s } a\up{in} (s)
    \\
    = \sqrt{ e^{ - 2 G t } } b (0)   - i  \sqrt{ 1 - e^{ - 2 G t }} A\up{in}.
\end{multline}

## Temporal modes

$a^\da(t_0) \ket 0$ describes a photon created at $t = t_0$.

$A^\da \ket 0 = \int_0^\tau \dd t f^* (t) a^\da (t) \ket 0$ describes a photon distributed over times according to $f (t)$.



## Temporal modes (discussion)

\begin{equation}
    b (t) = \sqrt{ T } b (0)  - i  \sqrt{ 1 - T} A\up{in}.
\end{equation}
where
\begin{equation}
    T = e^{ - 2 G t },
    \quad
    A\up{in} = \sqrt{ \frac{ 2 G }{ e^{ 2 G t  } -1  } } \int_0^t \dd s e^{ G s } a\up{in} (s).
\end{equation}

. . .

Transformation preserves commutation relations

\begin{equation}
    \comm{ b (t) }{ b^\da (t) } = 1.
\end{equation}

. . .

After infinite time, $T = 0$, and mechanical state is swapped out
\begin{equation}
    b(\infty) = i A\up{in}.
\end{equation}

$A\up{in}$ describes an effective oscillator whose state is written onto $b$.

State upload: target state has to be prepared in the mode $A\up{in}$.

## Light

We have solutions
\begin{align}
    b (t) = e^{ - G t  } b (0)  - i  \sqrt{ 2 G } e^{ - G t } \int_0 ^t \dd s e^{ G s } a\up{in} (s)
    \\
    a(t) = \frac{ - i g }{ \kappa } b(t)  + \sqrt{ \frac 2 \kappa } a\up{in} (t),
\end{align}

Can obtain $a(t)$ as a function of $b(0)$ and $a\up{in} (t)$.

## Output Light

\begin{equation}
    b (t) = e^{ - G t  } b (0)  - i  \sqrt{ 2 G } e^{ - G t } \int_0 ^t \dd s e^{ G s } a\up{in} (s)
\end{equation}
Input-output relation
\begin{equation}
    a\up{out} (t) = - a\up{in}(t) + \sqrt{ 2 \kappa } a (t),
\end{equation}
\begin{equation}
    a(t) = \frac{ - i g }{ \kappa } b(t)  + \sqrt{ \frac 2 \kappa } a\up{in} (t),
\end{equation}
\begin{multline}
    a\up{out} (t) = a\up{in} (t) - i \sqrt{ 2 G } b(t)
    \\
    = a\up{in} (t) - 2 G e^{ - G t } \int_0^t \dd s e^{ G s } a\up{in} (s) - \highlight{i \sqrt{ 2 G } e^{ - G t } b(0)}.
\end{multline}

## Homodyne detection

\begin{tikzpicture}
    \node [ minimum width = 7mm , minimum height = 7mm ] (bs) {} ;
    \node [ minimum width = 5mm , minimum height = 5mm , above = of bs ] (detN) {} ;
    \node [ minimum width = 5mm , minimum height = 5mm , right = of bs ] (detE) {} ;


    \node [ minimum size = 8mm , above right = of bs, draw , ultra thick, circle , inner sep = 0 ] (min) {\textbf{---}} ;

    \draw [ thick, fill = blue!40 , opacity = 0.2 ] (bs.south west) rectangle (bs.north east) 
    (bs.south west) -- node [at end , above right, text opacity = 1 , inner sep = 0] {$50\%$} (bs.north east) ;



    \draw [ very thick] (detN.south west) to [ out = 90, in = 90 , looseness = 3] (detN.south east) -- cycle ;
    \draw [ very thick] (detE.south west) to [ out = 0, in = 0 , looseness = 3] (detE.north west) -- cycle ;

    \draw [<-] (bs.west) -- node [above , midway] {$c\up{in}$} ++ (-1,0) ;
    \draw [<-, line width = 1mm] (bs.south) -- node [midway, left] {$C$} ++ (0,-1) ;

    \draw [->, ultra thick] (bs.north) -- node [ near end, right ] {$C + c\up{in}$} (detN) ;
    \draw [->, ultra thick] (bs.east) --  node [near end, below , outer sep = 1ex] {$C - c\up{in}$} (detE) ;


    \draw [ -> ] (detN.north) to [ out = 90 , in = 180 ] node [midway, above] {$i_+(t)$} (min) ;
    \draw [ -> ] (detE.east) to [ out = 0 , in = -90 ]  node [midway, right] {$i_-(t)$} (min) ;

    
\end{tikzpicture}

Homodyne current $i(t) = i_+ (t) - i_- (t) \propto C(t)  \Big[ c\up{in} (t) e^{ i \theta } + c\up{in}{}^\da (t) e^{ - i \theta } \Big]$

. . .

The current is collected
\begin{multline}
    \mathcal I = \int_0^\tau \dd t i(t)
    = \int_0^\tau \dd t C(t)  \Big[ c\up{in} (t) e^{ i \theta } + c\up{in}{}^\da (t) e^{ - i \theta } \Big]
    \\
    = \sum_k  \Delta \tau_k P_k C_k (t_k) \Big[ c\up{in} (t) e^{ i \theta } + c\up{in}{}^\da (t) e^{ - i \theta } \Big]
\end{multline}


*** 

Can measure different modes by modulating $C(t)$ or by sampling $i_k$ with weights $P_k$.

![](fig/decayLO.pdf "decaying lo")
![](fig/constLO.pdf "constant lo")

. . .

Importantly, have to preserve commutation relations when integrating $a$ into $A$:

\begin{equation}
    A = \int_T f(t) a(t) \dd t \Rightarrow \int_T | f(t) |^2 \dd t = 1.
\end{equation}

otherwise $\comm{ A }{ A^\da } \neq 1$.

## Example: single photon detection

Assume there is $\ket 1$ in the cavity at $t = 0$.

If the cavity is undriven, it decays according to 
\begin{equation}
    a(t) = a(0) e^{ - \kappa t } +  \sqrt{ 2 \kappa } e^{ - \kappa t } \int_0^t \dd s e^{ \kappa s } a\up{in} (s).
\end{equation}

The output field is then
\begin{equation}
    a\up{out} = - a\up{in} (t) + \sqrt{ 2 \kappa } a (t)  =  a\up{in}(t) [ \dots ] + \highlight{ a (0) e^{ - \kappa  t } }
\end{equation}

To most effectively detect $a(0)$, we measure
\begin{equation}
    A\up{out} = \int_0^\tau \dd s a\up{out} (s) e^{ - \kappa s } = \dots + \sqrt{ 1 - e^{ - 2 \kappa \tau }} a(0).
\end{equation}

Given sufficient time $\tau$, $A\up{out} = a(0)$.

## Readout of optomechanics

\begin{equation}
    \label{eq:aout}
    a\up{out} (t) = a\up{in} (t) - 2 G e^{ - G t } \int_0^t \dd s e^{ G s } a\up{in} (s) - \highlight{i \sqrt{ 2 G } e^{ - G t } b(0)}.
\end{equation}

Most effective detection of $b(0)$ is by
\begin{equation}
    A\up{out} \propto \int a\up{out} (t) e^{ - G t } \dd t
\end{equation}

. . .

After substitution
\begin{equation}
    A\up{out} = \sqrt{ e^{ - 2 G t }} A\up{in} - i \sqrt{ 1 - e^{ - 2 G t }} b(0).
\end{equation}

Use \eqref{eq:aout}, swap integration order to change $\int e^{ - 2 G t } \int a\up{in} \mapsto \int a\up{in} \int e^{ ..}$.


## Pulsed input-output relations

\begin{align}
    A\up{out} %& = \sqrt{ e^{ - 2 G t }} A\up{in} - i \sqrt{ 1 - e^{ - 2 G t }} b(0)
    = & \sqrt{ T } A\up{in} - i \sqrt{ 1 - T} b(0).
    \\
    b(t) % & = \sqrt{ e^{ - 2 G t } } b (0)   - i  \sqrt{ 1 - e^{ - 2 G t }} A\up{in}
    = & \sqrt{ T } b (0)   - i  \sqrt{ 1 - T } A\up{in},
\end{align}

where 
\begin{align}
    T & = e^{ - 2 G \tau },
    \\
    A\up{in} & = \sqrt{ \frac{ 2 G }{ e^{ 2 G \tau } - 1 } } \int_0^\tau \dd t a\up{in} (t) e^{ G t },
    \\
    A\up{out} & = \sqrt{ \frac{ 2 G }{ 1 - e^{ - 2 G \tau }} }  \int_0^\tau \dd t a\up{out} (t) e^{ - G t }.
\end{align}

We can understand these integrations as filtering of certain temporal modes.

## Fewer approximations

In general case (here $\vec R = ( a , b )$, $C$ is a matrix of coefficients)
\begin{equation}
    \dot{ \vec R } =  C \vec R + \vec \nu,
\end{equation}
can be solved
\begin{equation}
    \vec R (t) = M(t).\vec R (0) + \int_0^t \dd s M ( t - s ).\vec \nu (s).
\end{equation}
where $M(t) = e^{ C t }$.


At the end of the day
\begin{equation}
    a\up{out} (t) = \dots + M_{12} (t) b(0) + \dots.
\end{equation}

Apparently, the optimal choice for detection is 
\begin{equation}
    A\up{out} = \int a\up{out} (t) f\up{out} (t),
\end{equation}
with
\begin{equation}
    f\up{out} \propto M_{12} (t).
\end{equation}

## Input-output relations (general case)

\begin{align}
    b\s m (\tau) & = [\dots] b\s m (0) +  [\dots] a\s{c} (0) + [\dots] A\up{in} + [\dots] b\up{th} 
    \\
    A\up{out} & = [\dots] b\s m (0) +  [\dots] a\s{c} (0) + [\dots] A\up{in} + [\dots] b\up{th} 
\end{align}

Usually we know statistics of everything on the right, hence can evaluate statistics on the left

# Optomechanical transduction

## Example system 

Optomechanical cavity that supports two optical modes

\begin{multline}
    \frac 1 \hbar H = \omega_1 (x) a_1^\da a_1 + \omega_2 (x) a_2^\da a_2
    \\ = 
    \omega_1  a_1^\da a_1 + \omega_2  a_2^\da a_2 - g_{01}  a_1^\da a_1 (b\s m + b\s m^\da) - g_{02}  a_2^\da a_2 (b\s m + b\s m^\da).
\end{multline}

Under strong drive the interaction linearizes
\begin{equation}
    \frac 1 \hbar H = 
    \omega_1  a_1^\da a_1 + \omega_2  a_2^\da a_2 - g_1 (t) ( a_1 + a_1^\da ) (b\s m + b\s m^\da) - g_2 (t) ( a_2 + a_2^\da ) (b\s m + b\s m^\da).
\end{equation}
where
\begin{equation}
    g_i (t) = g_{0i} \sqrt{ \bar n\s{cav}{}_i (t)}
\end{equation}
is the coupling strength enhanced by the strong coherent drive.

## Pulsed transduction

Assume
\begin{equation}
    n\s{cav}{}_1 (t) = n_1 , 0 \leq t \leq \tau_1,
    \qquad
    n\s{cav}{}_2 (t) = n_2 , \tau_1 \leq t \leq \tau_1 + \tau_2,
\end{equation}
also assume $n\s{cav}{}_i = 0$ else.

. . .

Assume both drivings at lower mechanical sidebands: $\omega\s{d}{}_i = \Omega\s{cav}{}_i - \omega\s{m}$

There will be two subsequent state swap interactions
\begin{tikzpicture}
    \draw [->] (0,-2mm) -- node [at end, above right ] {$t$} ++ (8,0) ;


    \draw [ cyan ] (1,0) -- ++ (0,1) -- node [midway , above ] {$n_1$} ++ (3,0) -- ++ (0,-1) ;
    \draw [ magenta ] (4.1,0) -- ++ (0,1) -- node [midway , above ] {$n_2$} ++ (3,0) -- ++ (0,-1) ;
\end{tikzpicture}

## Input-Output relations

After first interaction
\begin{align}
    b\s m (\tau_1) & = \sqrt{ T } b_m (0) + \sqrt{ 1 - T } A_1\up{in} \mapsto A_1 \up{in},
    \\
    A_1\up{out} & = b_m (0).
\end{align}
After the second interaction
\begin{equation}
    b\s{m} (\tau_1 + \tau_2) = A_2\up{in},
    \quad
    A\up{out}_2  = b\s{m} (\tau_1) = A_1 \up{in}.
\end{equation}

The state $A_1\up{in}$ is transferred to $A_2\up{out}$.
. . .

Optionally, add third interaction between mechanics and $A_1$.

After:
\begin{equation}
    b\s m (\tau_1 + \tau_2 + \tau_3 ) = b\s m (0),
    \quad
    A_1\up{out} = A_2 \up{in}.
\end{equation}

# Transducer evaluation

## Transducer 

- Transducer maps a quantum state onto a different mode.
- Optionally, a transducer performs a state swap between the modes.

- Let's focus on a one-directional map (interface)

## Figure of merit

Ideal map from $(q,p)$ to $(x,y)$
\begin{equation}
    x = q, \quad y = p.
\end{equation}

Non-ideal map is characterized by loss $\eta$ / transmittance $T$ and noise $(q,p)\s N$
\begin{align}
    x & = \sqrt{ T } q + \sqrt{ 1 - T } q\s N,
    \\
    y & = \sqrt{ T } p + \sqrt{ 1 - T } p\s N
\end{align}

Added noise variance 
\begin{equation}
    V\s N = \sqrt{ \text{Var} q\s N \times  \text{Var} p\s N } \geq \sigma\s{vac}.
\end{equation}

Important property of the interface 
\begin{equation}
    V\s N (T)
\end{equation}

Typically $V\s N \propto \frac{ \text{const}}{ 1 - T }$.


## Imperfections

- thermal noise (increase $V\s N$)
- imperfect coupling (reduce $T$)

$\kappa = \kappa\s{ext} + \kappa\s{int}$

$\kappa\s{ext}$ --- coupling to detection channel

$\kappa\s{int}$ --- photon losses (scattering, unwanted transmission through mirrors that should be perfectly reflective, etc.)

Langevin equation
\begin{equation}
    \dot a = - \kappa a + \sqrt{ 2 \kappa\s{ext} } a\up{in} + \sqrt{ 2 \kappa\s{int} } a\up{Noise}
\end{equation}

Input-output relation
\begin{equation}
    a\up{out} = - a\up{in} + \sqrt{ 2 \kappa\s{ext}} a
\end{equation}

## Applications

One of the modes can be microwave, one optical

![Electro-opto-mechanical transducer](./fig/fig_6_lehnert_eom_transd.png "EOM TR"){ width=60% }

Helpful for both sides: 

- optical tomography for microwaves
- optical telecommunications for superconducting q. computing
- mw nonlinearities for optics




# Optomechanical toolbox

## Summary

### In red detuning ($\omega\s d = \Omega\s{cav} - \omega\s m$)

$$H\s{int} = g ( a\s c b\s m ^\da + \hc)$$

. . .

### In blue detuning ($\omega\s d = \Omega\s{cav} + \omega\s m$)

$$H\s{int} = g ( a\s c b\s m  + \hc)$$
