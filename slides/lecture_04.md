---
title: Quantum Optomechanics. Lecture 4.
subtitle:  Radiation pressure coupling.  Hamiltonian Formulation.  Important applications
fontsize: 10pt
fontfamily: libertine
author: A. Rakhubovsky
date: 21.10.2020
institute: UPOL
---

## The contents

- two ways of deriving the optomechanical interaction
    - semi-classical
    - quantum Hamiltonian formulation

# Hamiltonian formulation

## Fabry-Perot cavity

- single-mode cavity with movable mirror
    $$H = \hbar \Omega a^\da a$$

- frequency is a function of length
    $$\Omega = \frac{ \pi n c }{ L (x) } = \frac{ \pi n c }{ L_0 + x } = \frac{ \pi n c }{ L_0 } \frac{ 1 }{ 1 + x/L_0} \approx \Omega_0 \Big( 1 - \frac{ x }{ L_0 } \Big) $$

- approximate Hamiltonian
    $$H = \hbar \Omega_0 a^\da a - \frac{ \Omega_0 }{ L } x a^\da a = H_0 - G x n.$$

## Validity and meaning

- The Hamiltonian of the optomechanical interaction
    $$H\s{om} = - G x a^\da a.$$

- Participating modes come from the quantization of cavity and mechanical motion
    $$ \vec E ( \vec r , t ) = \sum  a (t) \vec u ( \vec r ) + \hc$$
    $$ \vec R ( \vec r , t ) = \sum  x (t) \vec u\s{m} ( \vec r ) + \hc$$

    - $x$ is the actual displacement of mirror from equilibrium
    - measured in meters (or other units of length)

- In reality more modes overlap and not the entire displacement $x$ contributes to frequency shift

## Quantum non-demolition photon number detection

- Non-demolition Hamiltonian
    $$\comm{H_0}{a^\da a} = \comm{H\s{om}}{a^\da a} = 0$$
- This means that addition of $H\s{om}$ to $H_0$ does not change the dynamics of $a^\da a$.
- We can (in principle) use the optomechanical coupling to measure photon number
- order-of-magnitude estimates (previous lecture)
- $F\s{mech} = \hbar \frac \Omega L a^\da a$
- few photons displace mechanical oscillator negligibly compared to thermal state

## What was the advantage of cavity?

- without the cavity the momentum transfer to mechanical oscillator per photon
    $$\Delta p\s{free} \propto \frac{ \hbar \Omega\s{L} }{ c } $$
- with the cavity the force
    $$F = \frac{ \partial H }{ \partial x } = \hbar \frac{\Omega\s{L}}{L} $$
- the momentum transfer $\Delta p = F \Delta \tau$
- reasonable choice is the inverse cavity linewidth $\kappa^{-1}$

- with cavity the momentum transfer increases by cavity *Finesse*
    $$\Delta p\s{cav} \propto \hbar \frac{ \Omega\s L}{L \kappa } = \frac{ \hbar \Omega }{ c } \frac{ c }{ L } \tau = \Delta p\s{free} \mathcal F, \quad \mathcal F = \Delta \Omega\s{FSR} / \kappa $$

## Units of measurement

$$H\s{om} = - G \times x \times a^\da a$$

-   instead of $x$ use dimensionless displacement
-   $x = x\s{zp} ( b + b^\da)$
-   in the Hamiltonian ($g_0 = G x\s{zp}$)
    $$ H\s{om} = - \hbar g_0 ( b + b^\da ) a^\da a.$$
-   $g_0$ ("g-naught") has the meaning of cavity frequency shift per phonon

## Linearization

- the interaction from nonlinear Hamiltonian is weak, need to enhance
    $$H\s{om} = - \hbar g_0 (b^\da + b ) a^\da a $$

- drive the cavity with coherent light $a \mapsto \alpha + a$
- mechanical mode is displaced correspondingly $b \mapsto \beta + b$
- formally: displaced frame defined by $H\s{df} = \alpha^* a + \beta^* b + \hc$
-
    $$H\s{om} = - \hbar g_0 ( 2 \Re \beta + b^\da + b )( \alpha + a )( \alpha^* + a)$$
- Nontrivial terms (assume $\alpha^* = \alpha$, what is $\alpha$ btw?)
    \begin{multline}
    H\s{om} = - \hbar g_0 ( b^\da + b )( | \alpha |^2 + \alpha ( a + a^\da) + a^\da a ) \mapsto
    \\
    - | \alpha |^2 x - g_0 \alpha x ( a + a^\da ) - g_0 a^\da a x \mapsto
    \\
    - g_0 \alpha (b + b^\da) ( a + a^\da )
    \end{multline}

## Step back: formalize the driving a bit

-   Classical driving of the cavity
    $$H = \hbar \Omega\s{L} a^\da a + \hbar \omega b^\da b - \hbar g_0 x a^\da a - i \hbar E ( e^{ - i \omega\s{d} t } a^\da - e^{ i \omega\s{d} t } a ).$$
-   Switch to frame that rotates at $H = \hbar \omega\s{d} a^\da a$ and introduce detuning $\Delta = \Omega\s{L} - \omega\s{d}$:
    $$H = \hbar \Delta a^\da a + \hbar \omega b^\da b - \hbar g_0 x a^\da a - i \hbar E ( a^\da - a ).$$
-   Further steps same as before: displace and linearize
-   Finally, the *full Hamiltonian, linearized*
    $$H = \hbar \Delta a^\da a + \hbar \omega b^\da b - \hbar g_0 ( b^\da + b )( \alpha a^\da + \alpha^* a).$$

## Validity and meaning again

-   The Hamiltonian
    $$H = \hbar \Delta a^\da a + \hbar \omega b^\da b - \hbar g_0 \sqrt{ n\s{cav} } ( b^\da + b )( \alpha a^\da + \alpha^* a).$$

-   Assumptions
    -   Strong driving at detuned frequency $\omega\s{d} = \Omega\s{L} - \Delta$
    -   The driving creates approximately a coherent state with $\alpha^2 = n\s{cav}$ inside the cavity
    -   Go to displaced frame to *re-center* at strong amplitudes, to focus at quantum fluctuations around those
    -   Linearize what remains

# Applications in resolved sideband regime

## Rotating frame (generally) 

Consider a Hamiltonian $H = H_0 + V$ that consists of a well-known part $H_0$ and a perturbation $V$ (no assumptions are done about the perturbation magnitude).

The Hamiltonians generate unitaries ($\hbar = 1$):
\begin{equation}
    H \mapsto U(t) = e^{ - i H t },
    \quad
    H_0 \mapsto U_0 (t) e^{ - i H_0 t },
\end{equation}

A general operator $A$ dynamics is determined by Heisenberg equation (and its solution)
\begin{equation}
    \dot A (t) = i \comm{ H }{ A };
    \Rightarrow
    A (t) = U^\da (t) A(0) U (t);
\end{equation}

Consider an operator  (note the order of $^\da$;  $U_0$ cancels the evolution due to $H_0$)
\begin{equation}
    \alpha(t) \equiv U_0 (t) A(t) U^\da_0 (t) = U_0 (t) U^\da (t) A(0) U (t) U^\da_0 (t).
\end{equation}

## Rotating frame (contd)

Derive equation of motion for $\alpha(t)$
\begin{equation}
    \dot \alpha = \dot U_0 A U_0^\da + U_0 \dot A U_0^\da + U_0 A \dot U_0^\da.
\end{equation}

By construction, $\dot U_0 = - i H_0 U_0$, also $\comm{ H_0 }{ U_0 } = 0$ (same with $U_0^\da$).

Substitute this
\begin{equation}
    \dot \alpha = - i \comm{ H_0 }{ \alpha } + U_0 \dot A U_0^\da
\end{equation}

The second term
\begin{multline}
    U_0 \dot A U_0^\da  = U_0  \left( i \comm{ H_0 }{ A } + i \comm{ V }{ A } \right) U_0^\da
    \\
    = i \comm{ H_0 }{  U_0 A U_0^\da } + i ( U_0 V U_0^\da U_0 A U_0^\da - U_0 A U_0^\da U_0 V U_0^\da )
    \\
    = i \comm{ H_0 }{ \alpha } + i \comm{ V' }{ \alpha }
\end{multline}

Together
\begin{equation}
    \dot \alpha = i \comm{ V' }{ \alpha },
    \quad
    \text{where}
    \quad
    V' = U_0 V U_0^\da.
\end{equation}

## Rotating frame (summary)

\begin{equation}
    H = H_0 + V,
    \quad
    U_0 = e^{ - i H_0 t },
    \quad
    \alpha = U_0 A U_0^\da.
\end{equation}

Operator $U_0$ applied this way ($\alpha = U_0 A U_0^\da$) cancels evolution due to $H_0$.  This way, $\alpha$ is "slow amplitude" with respect to dynamics driven by $H_0$.

**Equation of motion for $\alpha:$**
\begin{equation}
    \dot \alpha = i \comm{ V' }{ \alpha },
    \quad
    \text{where }
    V' = U_0 V U_0^\da.
\end{equation}

That is, to obtain equation of motion for $\alpha$, we 

-   drop $H_0$ 
-   transform $V$: if $V = \sum_k A^k ,\ U_0 V U_0^\da = \sum_k ( U_0 A U_0^\da )^k = \sum_k \alpha^k$.

**Same as if the Hamiltonian for $\alpha$ was just $V'$.**

Application $U_0 ( \cdot ) U_0^\da$ transforms $A \mapsto \alpha$, therefore $U_0 V U_0^\da$ replaces $A$ with $\alpha$ in $V$.

## Rotating frame (application)

Driven cavity:
\begin{gather}
    H = \hbar \Omega a^\da a + E ( e^{ - i \omega\s{d} t } a^\da + \hc),
    \\
    H_0 = \hbar \omega\s{d} a^\da a,
    \quad
    V = \hbar (\Omega - \omega\s{d}) a^\da a + E ( e^{ - i \omega\s{d} t } a^\da + \hc),
    \\
    U_0 = e^{ - i \omega\s{d} a^\da a }, \quad A(t) = a(t),
    \\
    U_0^\da a U_0 = a e^{ - i \omega\s d t }, \Rightarrow U_0 a U_0^\da = a e^{ i \omega \s d t },
    \\
    \alpha = U_0 a(t) U_0^\da ,
    \\
    U_0 V U_0^\da = \hbar \Delta a^\da a  + E ( U_0 a U_0^\da e^{ i \omega\s d t } + \hc ) = \hbar \Delta a^\da a + E ( a + a^\da ).
\end{gather}


## Rotating frame (optomechanics)

Hamiltonian of optomechanical cavity
    $$H = \hbar \Delta a^\da a + \hbar \omega b^\da b - \hbar g  ( b^\da + b )(  a^\da + a). $$

-   go to rotating frame $H\s{rf}=  \hbar \Delta a^\da a + \hbar \omega b^\da b$
    -   this means removing these terms and replacing
    - $a\mapsto a e^{ - i \Delta t }, \quad b\mapsto b e^{ - i \omega t }$
-   The remaining terms are
    $$H = - \hbar g \Big[ a b e^{ - i ( \Delta + \omega) t } + a b^\da e^{ - i ( \Delta - \omega) t } + \hc \Big].$$

-   Now apply RWA

## Validity of Rotating Wave Approximation

![Sketch of an optomechanical system](./tikz/modes-sketch.pdf)

- typical scales $\omega_m ~ \sim \Delta \gg \kappa \gg g \gg \gamma\s{m}$
- thanks to $\omega\s{m} \gg \kappa$ RWA works
- we can safely ignore $e^{ 2 i \omega\s{m} t }$

## Red detuning

$$H = - \hbar g \Big[ a b e^{ - i ( \Delta + \omega) t } + a b^\da e^{ - i ( \Delta - \omega) t } + \hc \Big].$$


-   Drive on the lower mechanical sideband, that is $\Delta = \Omega\s{L} - \omega\s{d} = \omega$
-   Then $\Delta + \omega = 2 \omega$,  $\Delta - \omega = 0$.
-   In RWA, the Hamiltonian
    $$H = - \hbar g ( a^\da b + a b^\da)$$
-   This is the state swap Hamiltonian

-   Applications
    -   state transfer
    -   cooling of the mechanical mode


## Blue detuning

$$H = - \hbar g \Big[ a b e^{ - i ( \Delta + \omega) t } + a b^\da e^{ - i ( \Delta - \omega) t } + \hc \Big].$$


-   Drive on the upper mechanical sideband, that is $\Delta = \Omega\s{L} - \omega\s{d} = - \omega$
-   Then $\Delta - \omega = 2 \omega$,  $\Delta + \omega = 0$.
-   In RWA, the Hamiltonian
    $$H = - \hbar g ( a^\da b^\da + a b)$$
-   The two-mode squeezing Hamiltonian
-   Applications
    - entanglement
    

# Radiation pressure: semiclassical treatment

## Driven cavity (proof of validity)

![cavity](./tikz/fig_compl_ampl.pdf)

-   Transmittivity and reflectivity $T^2 + R^2 = 1$
-   $\delta \tau$ is the phase shift from traveling one way between the mirrors $A\up{r} = e^{ - i \delta \tau } A\up{c}$
-   $A\up c = T A\up{in} + T A\up{in}(  R e^{ i 2 \delta \tau } ) + T A\up{in}(  R e^{ i 2 \delta \tau } )^2 + \dots = T A\up{in} \frac{ 1 }{ 1 - R e^{ i 2 \delta \tau}} = \frac{ T A\up{in}}{ 1 - ( 1 - \frac{ T^2 }{ 2 })( 1 + i 2 \delta \tau ) } = \frac{ T A\up{in}}{ \frac{ T^2 }{ 2 }- 2 i \delta \tau} = \frac{ \sqrt{ 2 \kappa \tau }}{ \kappa - i \delta } A\up{in} .$
-   $\kappa = \frac{ T^2 }{ 4 \tau }$.


## Pressure of light

![Reflection](./tikz/fig_displacement.pdf)

- Radiation pressure force $\propto A^2$
- if the mirror is still, $A\up{o} = A\up{i}$
- when the mirror displaces, $A\up{o} = A\up{i} e^{ 2 i k x } = A\up{i} ( 1 + 2 i k x )$
- additional radiation pressure (linear in $x$), $\Delta F = A\up{i} 2 k x \propto x$
- spring force $F = - K x$ (Hooke's law)

## Optical Spring and Viscosity

- Radiation pressure force $\propto x$
- actually $f(t) \propto x(t - \tau)$
- the force $f(t) = - K x (t - \tau) \approx - K x(t) + K \tau \dot x(t) = - K x + H \dot x$.
- the equation of motion
\begin{equation}
    m \ddot x + h \dot x + k x = - K x + H \dot x.
\end{equation}
- Viscosity has opposite sign
- Both effects depend on the sign of detuning

## The sign of detuning

![Optical Spring at different detunings](./tikz/slopes.png)
