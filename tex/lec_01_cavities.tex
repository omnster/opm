The goal of this lecture is to reiterate the necessary knowledge from quantum mechanics and illustrate it using optical cavities.

\section{Basic aspects of quantum mechanics} % <<<
\label{sec:basic_aspects_of_quantum_mechanics}

\subsection{Description of quantum systems and their dynamics} % <<<
\label{sec:description_of_quantum_systems}

In this course, our primary goal is going to be studying evolution of \emph{open quantum systems}.
To do so, we first have to understand what does it mean, to study such an evolution.
In particular, we have to understand how to describe a quantum system.

For example, in classical physics, the state of a particle that can perform one-dimensional motion is fully given by the instantaneous values of its position $q$ and velocity $\dot q$ (equivalently, position and momentum $(q,p)$).
Such a description thus requires only two real numbers.
A full description of motion is given by providing these two quantities (position and velocity/momentum) as functions of time.
In a more general case of multidimensional motion, the position is substituted by a vector of coordinates, and the momentum by the vector of generalized momenta.
The full description of a multidimensional motion of a classical particle requires providing these two vectors as functions of time:
\begin{equation}
  t \mapsto \left( \mvec r (t) , \mvec p_r (t) \right).
\end{equation}

In quantum physics, the definite values of positions and momenta apparently do not exist, but instead, one can specify the \emph{quantum state} of a system.
The quantum state of a closed system (the one that doesn't interact with any external systems), is given by a \emph{pure} state that is a vector in a corresponding Hilbert space.%
\footnote{We denote vectors using Dirac's 'bra-ket' notation: $\ket{\psi}$ is a vector.
Its dual $\bra{\psi}$ is a linear form that allows to construct a scalar product: given two vectors $\ket{ \phi }$ and $\ket{ \psi }$, their scalar product is given by $\braket{ \phi }{ \psi } = \braket{ \psi }{ \phi }^*,$ where a star denotes complex conjugation.
The scalar product has all standard properties, importantly, it is linear in both arguments.
}

A vector can be expanded in a preferred basis, and the coefficients of this expansion fully specify the vector, e.g.
\begin{equation}
  \ket{\psi} = \int_X \dl{x} \:  \braket{ x }{ \psi } \ket{ x } = \int_X \dl{x} \: \psi_x (x ) \ket{ x }.
\end{equation}
Here $x$ labels the basis states that span the set $X$.
One familiar choice of basis is the position basis, in which case $\ket{x}$ is a position eigenstate for each $x \in X \equiv \mathbb R$, and $\psi_x(x) \equiv \braket{ x }{ \psi }$ is the familiar wave function in the position basis.
Note that in this notation, $\psi_x (x_0)$ has a meaning of the coefficient of expansion of the wave-function over the (position) basis vectors.
Therefore, providing a wave-function in a closed form is somewhat equivalent to specifying infinitely many values of these expansion coefficients.
Specifying these values as a function of time gives full information about the dynamics of the quantum system:
\begin{equation}
  t \mapsto \psi_x (x; t) \text{ for all } x \in X.
\end{equation}
Usually, \emph{Schrödinger equation} is used to describe the evolution of the wave-function:
\begin{equation}
  \diff{}{t} \ket{ \psi (t) } = \frac{ 1 }{ \ii \hbar } \oper H \ket{ \psi (t) }
  % \tag{Schrödinger equation}
\end{equation}
where $\oper H$ is the Hamiltonian operator of the system, $\hbar = \SI{1.05e-34}{m^2 kg / s^2 }$ is the reduced Planck's constant.
In this course we will unlikely use Schrödinger equation often.

When a system is \emph{open}, that is, it interacts with the environment, its state is described by a \emph{density matrix}, often denoted $\hat \rho$.
It can be equivalently well expanded over a preferred basis:
\begin{equation}
  \hat \rho = \int_{X^2} \dl{x} \dl{x'} \: \rho( x , x' ) \dyad{ x }{ x ' }.
\end{equation}
Note that in this notation, $\rho(x,x')$ is not quite a matrix.
A full specification of the dynamics requires providing the elements of $\hat \rho$ for all times
\begin{equation}
  t \mapsto \rho( x , x' ; t )
\end{equation}
and $x,x'$ spanning all possible values in $X$.

A density matrix of a closed state evolves in time following the von Neumann equation
\begin{equation}
  \label{eq:basics:vonneumann}
  \diff{ \hat \rho }{ t} = \frac{ \ii }{ \hbar } \comm{ \hat \rho }{ \oper  H },
\end{equation}
The von Neumann equation has a solution
\begin{equation}
  \hat \rho (t ) =  \oper U ( t , t_0) \hat \rho (t_0) \oper U\dg ( t , t_0),
  \text{ with }
  \oper U (t, t_0 ) = \ee^{ - \ii \oper H ( t - t_0) }.
\end{equation}
The picture of quantum mechanics in which the quantum states evolve in time is called \emph{Schrödinger picture}.

In an experiment, neither wave-functions nor density matrices can be measured.
In contrast, the expectations of the system's \emph{observables} can be determined experimentally.
Observables are Hermitian operators that are, to some extent, quantum-mechanical analogies of classical functions.
The moments of the observable $\oper A$ can be computed knowing a density matrix
\begin{equation}
  \ev{ \oper A^k } = \Tr ( \hat \rho \oper A ^k ) \equiv \int_X \dl{x} \mel{ x }{  \hat \rho \oper A ^k  }{ x },
\end{equation}
where $\Tr$ is a trace operation.

It turns out that in many practical situations it is more convenient to specify a state $\hat \rho$ not by its elements, but rather by describing the statistics of a given set of observables.
One may notice that (denoting $\oper U = \oper U (t, t_0)$ for brevity)
\begin{equation}
  \ev{ \oper A } (t)
  = \Tr ( \hat \rho (t) \oper A )
  = \Tr ( \oper U \hat \rho (t_0) \oper U\dg \oper A )
  % \\
  = \Tr ( \hat \rho (t_0) \oper U\dg \oper A \oper U )
  = \Tr ( \hat \rho(t_0) \oper A\s{H} (t) ),
\end{equation}
where
\begin{equation}
  \oper A\s{H} (t) = \oper U\dg ( t, t_0) \oper A \oper U ( t, t_0)
\end{equation}
is the observable $\oper A$ in the Heisenberg picture.
In this picture, this observable obeys the Heisenberg equation of motion:
\begin{equation}
  \label{eq:basics:heisenbergeq}
  \diff{ \oper A }{ t } = \frac{ \ii }{ \hbar } \comm{ \oper A }{ \oper H }.
\end{equation}
We will not keep the subscript, as it is normally clear from the context which picture are we in.
In most cases, these equations of motion admit solutions which can be used to evaluate the statistics of the relevant observables as a function of time.

Summary:
\begin{itemize}
  \tightlist
  \item
  quantum states are described by density matrices or by statistics of observables
  \item
  the practical role of both (density matrix or statistics of observables) is to evaluate the statistics of the results of an experiment
  \item
  density matrix follows von Neumann equation~\eqref{eq:basics:vonneumann},
  observables follow Heisenberg equation~\eqref{eq:basics:heisenbergeq}
  \item
  solution of either equation allows to compute the desired statistics at later times $t > t_0$, given that the state at the initial time $t_0$ is properly specified (via the density matrix or via the observables).
\end{itemize}

\begin{example}[Coherent state]
  Borrowing from the future certain knowledge related to harmonic oscillators, we consider here the eigenstates of its annihilation operator $\oa$.
  For relevant definitions see
  % TODO: add references to relevant sections.

  Such eigenstates are called \emph{coherent} states and are parametrized by complex numbers $\alpha$:
  \begin{equation}
    \op a \ket{ \alpha } = \alpha \ket{ \alpha }.
  \end{equation}

  The wave-vector of a coherent state can be written in e.g. the basis of Fock states $\ket{k}$ (energy levels of a Harmonic oscillator), or position eigenstates:
  \begin{gather}
    \label{eq:example:coherent}
    \ket{ \alpha }
    =
    \sum_{ k = 0 }^\infty p_k \ket{k }
    =
    \int_{-\infty}^{\infty} \psi_{\ket{\alpha}} (x) \ket{x},
    \\
    \text{where }
    p_k = \exp\left[ - \frac{ \abs{ \alpha }^2 }{ 2 } \right]
    \frac{ \alpha^k }{ \sqrt{ k! }},
    \text{ and }
    \psi_{\ket{\alpha}}(x) \propto \exp\left[ - \frac{( x - \alpha_x )^2 }{ 4}  + \ii \alpha_p x \right].
    % \tag*{Coherent state}
  \end{gather}
  Here $\alpha_x = 2 \Re \alpha$, $\alpha_p = 2 \Im \alpha$, $\ox = \oa + \oa\dg$, and $\ket{x}$ is an eigenstate of $\ox$ with eigenvalue $x$:  $\ox \ket{x} = x \ket{x}$.

  \cref{eq:example:coherent} can be used to determine the desired statistics of any observables.

  Another way to describe a coherent state is to specify the statistical moments of the position and momentum ($\oper p = ( \oa - \oa\dg)/\ii$) operators.
  Since coherent state is a Gaussian state, it it sufficient to specify only the first moments (mean values) and the second moments (covariance matrix).
  For a coherent state, the mean values are equal to $\mvec \mu_\alpha = 2 ( \Re \alpha , \Im \alpha )$, and the covariance matrix equals an identity matrix $\mmat V_\alpha = \hat 1_2$.
  % TODO: define identity operator
\end{example}



% >>>

\subsection{Harmonic oscillator: Hamiltonian and quadratures} % <<<
\label{sec:harmonic_oscillator_hamiltonian_and_energy_levels}


Let us consider a particle of mass $\mu$ able to move along $x$-axis in a potential $V(x)$.
The classical Hamiltonian of such particle reads
\begin{equation}
	\label{eq:class:hamiltonian}
	H = \frac{ P^2 }{ 2 \mu } + V (X),
\end{equation}
where $P$ is the canonical momentum associated with the motion along $X$.
Near the point of equilibrium $X_0$ (we assume such point exists), one can expand the potential as
\begin{equation}
	V(X_0 + X ) = V(X_0) + V'(X_0) X + \frac 12 V''(X_0) X^2 + o (X^2) = V (X_0) + \frac 12 \varkappa X^2 + o (X^2).
\end{equation}
Here we used the property that at the equilibrium $V'(X) = 0$ and introduced the stiffness of the potential near the equilibrium $\varkappa = V''(X_0)$.
For simplicity, we can also assume that $V(x_0) = 0$, without any loss of generality.


After we displace the origin of the coordinate to the potential minium, and count energy relative to $V(X_0)$, the Hamiltonian reduces to the familiar form
\begin{equation}
	\label{eq:class:oscillator}
	H\s{cl} = \frac{ P^2 }{ 2 \mu } + \frac 12 \varkappa X^2 = \frac{ P^2 }{ 2 \mu } + \frac 12 \mu \Omega^2 X^2.
\end{equation}
We introduced here the usual eigenfrequency of the oscillator $\Omega = \sqrt{ \varkappa / \mu }$.

The Hamiltonian~\eqref{eq:class:oscillator} describes a \idef{harmonic oscillator}.
The transformations from~\cref{eq:class:hamiltonian} to~\cref{eq:class:oscillator} show that sufficiently small excursions around a stable equilibrium point of arbitrary system can always be described by the harmonic oscillator model.
The rare exceptions to this are situations where $V''(X_0) \equiv 0$, however, we will not consider these situations in this course.

The quantization of a mechanical object (such as the oscillator) means replacing classical functions $X$ and $P$ with Hermitian operators $\oper X$ and $\oper P$ that satisfy commutation relation
\begin{equation}
	\comm{\oper X }{ \oper P } = \oper X \oper P - \oper P \oper X = \ii \hbar.
\end{equation}
The number $\hbar =  \SI{1.05e-34}{\metre^2 \kilogram \per \second}$ is the reduced Planck's constant.
The operators $\oper X$ and $\oper P$ are called \idef{canonical quadratures} of an oscillator.

Non-commutativity of $\oper X$ and $\oper P$ causes the Heisenberg uncertainty relation
\begin{equation}
  \label{eq:heisenberg_unc}
	\sigma_X \sigma_P \geq \abs{ \comm{\oper X}{\oper P}  /2 }^2 = \frac{ \hbar^2 }{4} ,
\end{equation}
where $\sigma_\bullet$ means variance:
\begin{equation}
	\sigma_A
  = \avg{ \left( \oper A - \avg{\oper A} \right)^2 }
  = \avg{ \oper A^2} - \avg{ \oper A }^2.
\end{equation}
Taking the expectation here is meant with respect to a certain quantum state, say $\ket{\psi}$.
Importantly, \cref{eq:heisenberg_unc} holds for an \emph{arbitrary} quantum state, that is the product of the variances of canonically conjugate variables is bounded from below at arbitrary quantum states.

Note that, strictly speaking, taking a variance requires to define the quantum state with respect to which all the expectations are taken.
We will continue assuming that such state will be clear from the context, and specify it exactly when needed.

From this follows the existence of the lowest boundary for the HO's mean energy.
Indeed, we note that $\avg{ Q^2 } \geq \sigma_Q$ (for $Q = X, P$) and therefore
\begin{equation}
  \avg{ H\s{HO} } = \frac{ \oper P^2 }{ 2 \mu } + \frac 12 \mu \Omega^2 \ev{ \oper X^2 }
  \geq \frac{ \sigma_P }{ 2 \mu } + \frac 12 \mu \Omega^2 \sigma_X
  \geq 2 \sqrt{ \frac{ \mu \Omega^2 }{ 4 \mu }\sigma_P \sigma_X } = \frac{ \hbar \Omega }{ 2 },
\end{equation}
where we used $a + b \geq 2 \sqrt{ a b } $.

The state with minimal energy is the \idef{ground state} of a harmonic oscillator (usually referred to as the \idef{vacuum state} when speaking of a field oscillator).
The ground state is usually denoted $\ket{0}$ or $\ket{\text{vac}}$.
The variances of the quadratures evaluated in this state read
\begin{align}
	\sigma_P\up{ZP} &  = \frac{ \hbar \mu \Omega }{2 },
  &
	\sigma_X\up{ZP} &  = \frac{ \hbar }{ 2 \mu \Omega }.
\end{align}

A convenient next step that is normally done, is to switch to the dimensionless position and momentum $\oper x$ and $\oper p$ that are defined in the units of zero-point fluctuations:
\begin{align}
  X & = \oper x \sqrt{ \sigma_X\up{ZP} },
    &
	P & = \oper p \sqrt{\sigma_P\up{ZP}}.
\end{align}
It is easy to see that the canonical commutation relation written in the new quadratures reads
\begin{equation}
  \comm{ \oper x }{ \oper p } = 2 \ii.
\end{equation}

In the new quadratures,
\begin{equation}
	H = \frac{ \hbar \Omega }{ 4 } ( \oper x^2 + \oper p^2 ),
\end{equation}
and the variances of the new operators $ \oper x$, $\oper p$ in ground state both equal one:
\begin{equation}
	\sigma_{\oper p} = \sigma_{\oper x} = 1.
\end{equation}
These variances are colloquially termed \emph{ground-state variances} or \emph{shot-noise variances}\index{shot-noise variance}.

\begin{remark}
The choice of the normalization is very important in quantum physics, and it sets many important numerical bounds for different parameters.
For example, a quantum state that exhibits a variance of a canonical quadrature below the shot-noise level, is certainly non-classical according to the Glauber-Sudarshan definition of non-classicality.
Entanglement of Gaussian states is verified by comparing the corresponding symplectic eigenvalues with shot-noise variance.
\end{remark}


A different convention in frequent in quantum optics.
It is to define $\underline{\oper x} $ and $\underline{\oper p}$ such that
\begin{align}
	\comm{\underline{\oper x }}{\underline{\oper p }} & = \ii;
  &
	H & = \frac{\hbar \Omega}{2} ( \underline{ \oper x}^2 + \underline{ \oper p}^2 );
  &
	\sigma_{\underline{\oper x }} & = \sigma_{\underline{\oper p }} = \tfrac 14 .
\end{align}



% >>>

\subsection{Harmonic oscillator: Energy levels} % <<<
\label{sec:harmonic_oscillator_energy_levels}

The energy levels of a Harmonic oscillator can be found from the stationary Schrödinger equation
\begin{equation}
  \oper H \ket{ \psi } = \frac{ \hbar \Omega }{ 2 } ( \oper x^2 + \oper p^2 ) \ket{ \psi }= E \ket{ \psi },
\end{equation}
where $E$ is the numerical value of the energy.
This equation can be solved as a differential equation for the level $\ket{ \psi}$ in a certain basis.
To do so, we first have to determine representations of the canoncai quadrature operators.

In the basis of the dimensionless position operator $\oper x$, its action on a quantum state is equivalent to multiplication by $x$: $\mel{ x }{ \oper x }{ \psi } = x \braket{ x }{ \psi }$.
The action of the momentum operator is less trivial, but one can show that
\begin{equation}
  \mel{ x }{ \oper p }{ \psi } = - 2 \ii \: \partial_x  \braket{ x }{ \psi }.
\end{equation}
Or, more generally,
\begin{equation}
  \mel{ x }{ \oper p }{ \psi } = - \comm{\oper x }{ \oper p } \: \partial_x  \braket{ x }{ \psi }.
\end{equation}

Using these representations of the canonical quadratures, one can convince herself that if $\ket{\psi_0}$ is an energy level of an oscillator, then
\begin{equation}
  \label{eq:practicalrecipe:fock}
  \ket{ \psi_k } \propto ( \oper x - \ii \oper p )^k \ket{ \psi_0}
\end{equation}
is also an energy level.
It turns out, that the energy levels of a harmonic oscillator form a basis of equally spaced levels, normally called \idef{Fock states}.
The Fock states are indexed by an integer index and are usually denoted as $\ket{k}$ with integer values $k$.

Transition between the levels can be performed using the so-called \emph{creation}\index{creation operator}~($a\dg$) and \emph{annihilation}\index{annihilation operator}~($a$) operators defined as follows:
\begin{align}
  \oper a\dg & = ( \oper x - \ii \oper p ) / 2,
  &
  \oper a & = ( \oper x + \ii \oper p )/2,
  &
  \text{ consequently } \comm{ \oper a }{ \oper a\dg } = 1.
\end{align}
The two operators are also occasionally called \idef{bosonic operators} or \idef{ladder operators} of a harmonic oscillator.
It can be noted that in the classical picture, the operator $\oper a$ would describe a simple complex amplitude of an oscillator.
The operators $\oper a$, $\oper a\dg$ act on the Fock state as follows:
\begin{align}
  \oper a \ket{ n } & = \sqrt{ n } \ket{ n -1 },
  &
  \oper a\dg \ket{ n -1 } & = \sqrt{ n } \ket{ n }.
\end{align}
Expressed in the bosonic operators, the Hamiltonian of a harmonic oscillator reads $\oper H = \hbar \Omega(  \oper a\dg \oper a + \frac 12 )$.
The eigenstates (Fock states) of the Harmonic oscillator obey $\oper H \ket{n} = \hbar \Omega ( n + \frac 12 ) \ket{n }$.

Summary of~\cref{sec:harmonic_oscillator_hamiltonian_and_energy_levels,sec:harmonic_oscillator_energy_levels}.
\begin{itemize}
  \item We use the position ($\oper x$) and momentum ($\oper p$) quadrature operators normalized such that
  \begin{equation}
    \comm{ \oper x }{ \oper p } = 2 \ii,
  \end{equation}
  which implies $\oper p  = - 2 \ii \partial_{x }$.
  \item In this notation, the Hamiltonian of the harmonic oscillator reads $\oper H\s{HO} = \frac 14 \hbar \Omega ( \oper x^2 + \oper p ^2 ) $.
  \item The eigenstates (energy levels) of the Hamiltonian are the \emph{Fock states} $\ket{k}$ for integer $k \geq 0$: $\oper H\s{HO} \ket{k} = \hbar \Omega ( k + \frac 12 ) \ket{k}$.
  \item In the basis of the position operator $\oper x$ (with the normalization that we use), the vacuum (ground-state) wave-vector reads (up to a phase factor)
  \begin{equation}
    \braket{ x }{ 0 } = \frac{ 1 }{ \sqrt[4]{2 \pi} }
    \exp\left[ - \frac{ x^2 }{ 4 } \right].
  \end{equation}
  \item Jumps between the energy levels of the harmonic oscillators are performed by actions of creation and annihilation operators:
  \begin{align}
    \oper a\dg & = ( \oper x - \ii \oper p ) / 2,
    &
    \oper a & = ( \oper x + \ii \oper p )/2.
  \end{align}
\end{itemize}

\begin{remark}
  Note that the choice of normalization for the quadratures does not influence the properties of the bosonic operators.
  Whatever is the right-hand side of the canonical commutation relation of the quadratures, the commutation of the ladder operators is the same: $\comm{\oper a}{\oper a\dg} = 1$.
  Same is true regarding the Hamiltonian: it always equals $\hbar \Omega ( \oper a\dg \oper a + \sfrac 12 )$.
\end{remark}


% >>>

\subsection{Certain special states of harmonic oscillators.  Gaussian quantum states} % <<<
\label{sec:certain_special_states_of_harmonic_oscillator}

In this section, we provide examples of a few important quantum states that quite often occur when dealing with harmonic oscillators.

First, the eigenstates of the harmonic oscillator Hamiltonian, or the Fock states $\ket{ k}$ where $k \in \mathbb{N}_0$ (non-negative integers).
These states have wave functions
\begin{equation}
  \braket{ x }{ k } =  \psi_k (x)
  = N_k \exp\left[ - \frac{ x^2 }{ 4 } \right]
  H_k \left(  \frac{ x }{ \sqrt{ 2 } } \right),
\end{equation}
where $N_k$ are normalization constants, and $H_k$ are Hermite polynomials.

An important class of the quantum states of a harmonic oscillator are the so-called \idef{Gaussian states}.
These are the states $\rho$ such that their characteristic function (defined as $\chi_\rho (\alpha) = \Tr \left( \rho \exp\left[ \alpha^* a - \alpha a\dg \right] \right)$ where $\alpha$ is a complex number parametrizing the phase space) being a Gaussian function of the arguments.
The outstanding feature of the Gaussian states is that they are fully defined by the first two moments of the quadrature operators.
These are the vector of mean values (means) $\mvec \mu$ and the covariance matrix (CM) $\mmat V$.
Collecting the quadratures into a vector $\oper { \mvec r } = ( \oper x , \oper p )^\intercal$, we can write for these moments
\begin{align}
  \label{eq:Gaussian:moms:def}
  \mvec \mu_\rho
  = \ev{ \oper { \mvec r } }_\rho
  & =
  \begin{pmatrix}
    \ev{ \oper x }_\rho \\ \ev{ \oper p }_\rho
  \end{pmatrix}
  ,
  &
  \oper { \mvec \delta }_\rho & = \oper{ \mvec r } - \mvec \mu_\rho,
  =
  \begin{pmatrix}
    \oper{ \delta } x_\rho  \\ \oper { \delta } p_\rho
  \end{pmatrix},
  &
  \mmat V_\rho & =
  \ev{ \oper{ \mvec \delta }_\rho \circ \oper{ \mvec \delta }_\rho ^\intercal }
  =
  \begin{pmatrix}
    \ev{ \oper{ \delta } x_\rho ^2 }
    &
    \ev{  \oper{ \delta } x_\rho \circ \oper{ \delta } p_\rho }
    \\
    \ev{  \oper{ \delta } x_\rho \circ \oper{ \delta } p_\rho }
    &
    \ev{ \oper{ \delta } p_\rho ^2 }
  \end{pmatrix}.
\end{align}
Here $\ev{\blank \circ \blank}$ is the symmetrized mean: $\ev{a \circ b} = \frac 12 \ev{ a b + b a }$.

\begin{remark}
  In~\cref{eq:Gaussian:moms:def} we explicitly used the subscript $\rho$ to denote that the expectations are computed with respect to this state.
  In what follows we will often omit such subscripts for brevity, as the relevant quantum state should be clear from the context.
\end{remark}

The important Gaussian states of a quantum harmonic oscillator are coherent states, squeezed states, displaced squeezed states and thermal states.
A trivial Gaussian state (that formally belongs to each of the groups from the previous sentence) is the ground/vacuum state $\ket 0$.
This state has zero means and its covariance matrix is an identity matrix:
\begin{align}
  \mvec \mu_{\ket 0} & = \mvec 0,
  &
  \mmat V _{\ket 0} & = \eye_2.
\end{align}
Note that the CM has this simple shape only in our chosen notations ($\ox = \oa + \oa\dg$).
In different notations, the CM would have a prefactor.

\subsubsection{Coherent states $\ket{ \alpha }$} % <<<
\label{sec:coherent_states}

\emph{Coherent states}\index{coherent states} are the eigenstates of an annihilation operator.
They are normally labeled using the corresponding complex eigenvalue: $\oa \ket{ \alpha } = \alpha \ket {\alpha }$.
It is easy to show that for a coherent state
\begin{align}
  \vec \mu_{\ket{ \alpha}} & = 2 ( \Re \alpha , \Im \alpha )^\intercal,
  &
  \mmat V_{\ket{ \alpha}} & = \eye_2 .
\end{align}

The ground state $\ket{0}$ can be viewed in most cases as a coherent state with $\alpha =0$.

Coherent states are the result of applying displacement to the ground state:
\begin{equation}
  \ket{ \alpha }
  = \oper D ( \alpha ) \ket{ 0 }
  = \ee^{ \alpha \oper a\dg - \alpha^* \alpha } \ket{ 0 }.
\end{equation}
Ground state is formally a coherent state with a zero eigenvalue.

% >>>
\subsubsection{Squeezed states $\ket{\zeta}$} % <<<
\label{sec:squeezed_states_zeta}

\emph{Squeezed states}\index{squeezed states} are the result of applying a squeeze operator $\oper{S} ( \zeta )$ to the ground state

% TODO: - squeeze operator
% TODO: - covmat in general form
% TODO: - covmat for position/momentum squeezed states
% >>>
\subsubsection{Thermal states} % <<<
\label{sec:thermal_states}

Thermal states $\oper \rho\s{th} (m)$ are mixed states parametrized by their mean occupation $m = \ev{ \oper n }_{\oper \rho\s{th} (m)} = \Tr ( \oper \rho\s{s} (m ) \oper n)$.
For a given occupation number, the thermal state can be expanded in the Fock basis as
\begin{align}
  \oper \rho\s{th} (m) & = \sum_{n = 0}^\infty p\s{th} (n; m) \projector{n},
  &
  \text{where }
  p\s{th} (n; m) &= \frac{ m^n }{ ( 1 + m )^{n + 1 } }.
\end{align}
This is a Gaussian state with zero mean values of quadratures, and a covariance matrix proportional to the identity matrix:
\begin{align}
  \mvec \mu\s{th} (m) & = \mvec 0,
  &
  \mmat V\s{th} (m) & = ( 2 m + 1 ) \eye_2.
\end{align}


% >>>

% >>>

\subsection{Quantum dynamics. Changing frames} % <<<
\label{sec:quantum_dynamics_changing_frames}

Most practical tasks in quantum optics require predicting the statistics of the outcomes of an experiment.
Usually, one knows the \emph{inputs} of a quantum system (we will attempt to clarify the meaning of this further), at least its initial quantum state, and its Hamiltonian.
Using this knowledge, one normally has to compute the statistics (different moments) of a set of observables.

As said before, we can have the full information regarding a quantum state, by knowing its wave function.%
\footnote{In general, the wave function does not exist, and we need to know the density matrix.
However, in this section, for the brevity of notation, we will consider only pure states though all the conclusions remain valid for mixed states equivalently well.}
The dynamics of the wave function is described by the Schrödinger equation, which in the position basis reads
\begin{equation}
  \ii \hbar \diff{ \ket{ \Psi ( x , t ) }}{ t } = \oper H \ket { \Psi ( x , t ) },
\end{equation}
where we wrote explicitly the dependence on the position.
This is an ordinary differential equation (possibly, nonlinear) with respect to the wave-function.
In certain cases it can be solved using techniques of mathematical physics.
Then, using the initial condition $\ket{ \Psi ( x , t = 0 )} = \ket{ \Psi_0 (x) }$, one can compute the required statistics of the relevant operators.
Equivalently, one can write the Heisenberg equations for relevant operators
\begin{equation}
  \diff{ \oper A_i (t) }{ t } = \frac{ \ii }{ \hbar } \comm{ \oper H }{ \oper A_i (t) },
\end{equation}
and solve it with respect to the operators $\oper A_i(t)$ using the initial conditions $\oper A_i(t = 0) = \oper A_i^{(0)}$.

Both pictures can be interchanged in the language of the expectations of observables:
\begin{equation}
  \ev{ \oper A } (t) =
  \mel{ \Psi (t ) }{ \oper A\s{S }  }{ \Psi (t) } =
  \mel{ \Psi (0 ) }{ \oper U\dg (t) \oper A\s{S } \oper U(t)  }{ \Psi (0) } =
  \mel{ \Psi (0 ) }{ \oper A\s{H} (t) }{ \Psi (0) },
\end{equation}
with $\oper U (t) = \exp[ - \ii \oper H t / \hbar ]$.
Thus, regardless of the chosen representation, the dynamics of the system can be found by solving certain differential equations.

Quite often, these equations can be simplified by going into a different reference frame.
To illustrate, let us compare the dynamics of a state vector $\ket{\Psi}$ and this same vector in a different frame.
We denote the vector in a different frame by $\ket{ \Phi (t)}$, and the transformation between the frames
\begin{equation}
  \ket{ \Phi (t) } = \oper W (t) \ket{ \Psi (t) },
\end{equation}
where $\oper W (t)$ is a unitary operator
(meaning $\oper W \oper W\dg = \oper W\dg \oper W =  \eye$)
of the frame transformation.
The equation of motion for $\ii \hbar \ket{ \Phi }$ reads
\begin{multline}
  \ii \hbar \diff{ \ket{ \Phi (t) } }{t}
  =
  \ii \hbar \dot{ \oper W } \ket{ \Psi }
  +
  \oper W \ii \hbar \diff{ \ket{ \Psi }}{t}
  =
  \ii \hbar \dot{ \oper W } \oper W \dg \oper W \ket{ \Psi }
  +
  \oper W \oper H \ket{ \Psi }
  =
  \ii \hbar \dot{ \oper W } \oper W \dg \ket{ \Phi }
  +
  \oper W \oper H \oper W\dg \oper W \ket{ \Psi }
  \\
  =
  \left( \ii \hbar \dot{ \oper W } \oper W \dg + \oper W \oper H \oper W \dg \right) \ket{ \Phi }.
\end{multline}
By its structure, this equation happens to be the Schrödinger equation for the state vector $\ket{ \Phi }$ with the Hamiltonian
\begin{equation}
  \label{eq:hamiltonian:changeframe}
  \oper H' = \ii \hbar \dot{ \oper W } \oper W \dg + \oper W \oper H \oper W \dg.
\end{equation}
Finding a suitable $\oper W$ may considerably simplify the equations of motion for $\ket{\Phi(t)}$.
The obtained solution then can be used to compute the statistics of observables.

Assuming that the reason for transforming frames is finding the statistics of the operator $\oper A$, then going to a different frame, we perhaps have to transform this operator as well.
In the new frame it reads
\begin{equation}
  \oper A\s{S}' = \oper W \oper A\s{S} \oper W\dg.
\end{equation}
This can be seen from the reasoning that it has to satisfy
\begin{equation}
  \ev{ \oper A } (t) =
  \mel{ \Psi (t) }{ \oper A\s{S} }{ \Psi (t) } =
  \mel{ \Phi (t) }{ \oper A'\s{S} }{ \Phi (t) } =
  \mel{ \Psi (t) }{ \oper W\dg \oper W \oper A\s{S} \oper W\dg \oper W }{ \Psi (t) }.
\end{equation}

% TODO: check if this is correct.  Normally, the operators are transformed differently to the vectors.
% We can also consider the Heisenberg picture in the new frame.
% To this end, we denote the operator in the Heisenberg picture in the new frame as
% \begin{equation}
%   \oper A \s{H}' (t) = \oper W\dg (t) A\s{H} (t) \oper W  (t)
% \end{equation}
% and compute its derivative with respect to time.
% It reads (dropping the notation for the dependence on time for brevity: $[W(t) \mapsto W]$)
% \begin{multline}
%   \ii \hbar \diff{ \oper A \s{H}' (t) }{ t }
%   =
%   \ii \hbar \left(
%     \dot{ \oper W }\dg \oper A\s{H} \oper W
%     + \oper W\dg \dot{ \oper A}\s{H } \oper W
%     + \oper W\dg \oper A\s{H} \dot{ \oper W }
%   \right)
%   % \\
%   =
%   \ii \hbar \left(
%     \dot{ \oper W }\dg \oper A\s{H}    \oper W
%     + \oper W \dg \oper A\s{H} \dot{ \oper W }
%   \right)
%   + \oper W\dg \comm{ \oper A\s{H}}{ \oper H } \oper W
%   \\
%   =
%   \ii \hbar
%   \dot{ \oper W }\dg \oper W \oper W\dg \oper A\s{H} \oper W
%   +
%   \ii \hbar \oper W\dg \oper A\s{H} \oper W \oper W\dg \dot{ \oper W }
%   TUTU
%   \\
%   + \oper W\dg \oper A\s{H} \oper W \oper W\dg \oper H \oper W
%   -
%   \oper W \oper H \oper W\dg \oper W \oper A\s{H} \oper W\dg
%   \\
%   =
%   \ii \hbar \dot{ \oper W } \oper W\dg \oper A'\s{H}
%   -
%   \oper A\s{H}'
%   \left( \ii \hbar \dot{ \oper W } \oper W\dg \right)\dg
%   +
%   \comm{ \oper A\s{H}' }{ \oper W \oper H \oper W\dg }
%   % \\
%   = \comm{ \oper W \oper H \oper W\dg + \ii \hbar \dot{ \oper W } \oper W\dg }{ \oper A\s{H}' }.
% \end{multline}

% It is perhaps not surprising that we have once again arrived to a Heisenberg equation with the Hamiltonian $\oper H'$ given by~\cref{eq:hamiltonian:changeframe}.


\begin{example}[Rotating frame]
  Let's consider a situation when the Hamiltonian can be written as a sum of two terms
  \begin{equation}
    \oper H = \oper H_0 + \oper V,
  \end{equation}
  where the $\oper H_0$ part represents a known type of dynamics (e.g., we know the dynamics when $\oper V = 0$).
  Note that we do not make any assumptions that $\oper V$ ``is small''.

  One particularly useful choice of $\oper W$ is
  \begin{equation}
    \oper W(t) = \exp[ \ii \oper H_0 t / \hbar ].
  \end{equation}
  Note that when $\oper V = 0$, the full solution for $\Psi$ is $\ket{ \Psi(t) } = \exp[ - \ii \oper H_0 t / \hbar ] \ket{ \Psi (0)}$, and therefore, in this case, $\ket{ \Phi (t) } = \ket{ \Psi (0)}$.
  It should be clear then, that this choice of the operator $\oper W$ ``cancels'' the evolution described by $\oper H_0$.

  % TODO: write about operators
  % Similarly, when $\oper V = 0$, in the Heisenberg picture, $\oper A\s{H} (t) = \exp[ \ii \oper H_0 t / \hbar ] \oper A\s{S} \exp[ - \ii \oper H_0 t / \hbar ].$

  When $\oper V \neq 0$, we have
  \begin{equation}
    \oper H' =
    \oper W \oper H_0 \oper W\dg
    + \oper W \oper V \oper W\dg + \ii \hbar ( \ii \oper H_0 / \hbar )
    = \oper W \oper V \oper W\dg.
  \end{equation}
  Here we used that since $\oper W = \exp[ \ii \oper H_0 t / \hbar$, then $\comm{ \oper W }{ \oper H_0 } = 0$.

  Quite often, the Hamiltonian $\oper H_0$ can be chosen to describe  the evolution that is very familiar and has a known solution.
  The action of the transformation operator $\oper W$ on the perturbation Hamiltonian $\oper V$ can thus be computed straightforwardly.

  For example, for a harmonic oscillator, we normally choose $\oper H_0 = \hbar \omega \oa\dg \oa$ which has the familiar solution
  \begin{equation}
    \oa (t) = \ee^{\ii \oper H_0 t / \hbar} \oa (0) \ee^{  - \ii \oper H_0 t / \hbar  } =  \ee^{ - \ii \omega t } \oa (0).
  \end{equation}
  Therefore, choosing $\oper W = \exp[ \ii \oper H_0 t / \hbar ]$, we can write
  \begin{equation}
    \oper W\dg \oper a(t) \oper W
    =
    \ee^{- \ii \oper H_0 t / \hbar} \oa (t) \ee^{ \ii \oper H_0 t / \hbar  }
    \stackrel{*}{=}
    \ee^{ - \ii \omega t } \oa (t).
  \end{equation}
  Here, following the convention typical in quantum optics, the last instance of $\oa (t)$ relates to the new frame (normally called the \idef{rotating frame}).
  This is why the last equation sign is denoted with an asterisk.
\end{example}

% >>>

% >>>
\section{Damping: Quantum Langevin equations} % <<<
\label{sec:damping_quantum_langevin_equations}

Let us consider a single-mode cavity that is described by a simple Harmonic oscillator Hamiltonian
\begin{equation}
	H\s{sys} = \hbar \Omega a^\da a.
\end{equation}
The cavity is coupled to a bath that consists of an ensemble of harmonic oscillators described by bosonic operators $b _{\omega}$ with Hamiltonian
\begin{equation}
	H\s{bath} = \hbar \int \dd \omega  \omega b^\da _{\omega} b _{\omega},
\end{equation}
Individual modes of the bath are independent: $\comm{ b_{\omega} }{ b^\da _{\omega'} } = \delta ( \omega - \omega' )$.
The subscript $\omega$ here bears quite some meaning of enumeration of frequencies.
The system-bath coupling is of a photon-hopping type that is
\begin{equation}
	H\s{sb} = - i \hbar \int \dd \omega \tau (\omega) \left(  b _{\omega} a^\da - b^\da _{\omega} a \right).
\end{equation}
The compound system is driven by Hamiltonian
\begin{equation}
	H = H\s{sys} + H\s{bath} + H\s{sb}.
\end{equation}

Equations of motion in the Heisenberg picture for the operators read
\begin{align}
	\dot b_\omega & = \frac i \hbar \comm{ H }{ b_\omega } = - i \omega b_\omega + \tau (\omega) a,
	\\
	\dot a & = - i \Omega a - \int \dd \omega \tau (\omega) b_\omega.
\end{align}
Solution for $b_\omega$
\begin{equation}
	\label{eq:langevin:bomega}
	b_\omega (t) = e^{ - i \omega t } b_\omega (0) + \tau (\omega) e^{ - i \omega t } \int_0^t \dd s a (s) e^{ i \omega s }.
\end{equation}
Substituting this into the equation for $\dot a$, we obtain
\begin{multline}
	\dot a = - i \Omega a
	% \\
	- \int \dd \omega \tau (\omega) \left[  e^{ - i \omega t } b_\omega (0) + \tau (\omega) \int_0^t \dd s a (s) e^{ - i \omega ( t - s ) } \right]
	\\
	= - i \Omega a - \int \dd \omega \tau ( \omega) e^{ - i \omega t } b_\omega (0)
	%
	- \int_0^t \dd s a (s) \int \dd \omega \tau^2 (\omega) e^{ - i \omega (t -  s)}.
\end{multline}

\emph{First Markov approximation}\index{First Markov approximation} is an assumption that coupling  $\tau(\omega) = \sqrt{ \kappa / 2 \pi }$ is constant across frequencies.
Then
\begin{multline}
	\dot a = - i \Omega a - \sqrt\frac{ \kappa }{ 2 \pi } \int \dd \omega e^{ - i \omega t } b_\omega (0) - \frac{ \kappa }{ 2 \pi } \int_0^t \dd s a(s) \int \dd \omega e^{ - i \omega ( t - s )}
	\\
	= - i \Omega a (t) - \sqrt{ \kappa } b\up{in} (t) - \frac{ \kappa }{ 2 } a (t).
	\label{eq:langevin:theeq}
\end{multline}
where we have defined
\begin{equation}
	b\up{in} (t) = \frac{ 1 }{ \sqrt{ 2 \pi }}  \int \dd \omega e^{ - i \omega t } b_\omega (0),
\end{equation}
and used
\begin{equation}
	\int \dd \omega e^{ - i \omega ( t - s )} = 2 \pi \delta (t - s), \text{ and } \int _0^t \dd s  a(s) \delta ( t - s ) = \frac 12 a (t).
\end{equation}

\cref{eq:langevin:theeq} is called \emph{Heisenberg-Langevin} equation\index{Heisenberg-Langevin equation}.
The quantity $\kappa$ is called the cavity linewidth.

Inverting time in~\cref{eq:langevin:bomega}, one can also define
\begin{equation}
	b\up{out} (t) = \frac{ 1 }{ \sqrt{ 2 \pi }}  \int \dd \omega e^{ - i \omega (t - t') } b_\omega (t'),
\end{equation}
where $t'$ is an instant in future.
Carrying similar considerations, one can obtain a time-reversed Langevin equation
\begin{equation}
	\dot a = - i \Omega a + \frac{ \kappa }{ 2 } a - \sqrt{ \kappa } b\up{out}.
\end{equation}
from which we can connect the leaking field $b\up{out}$ with the incoming and the intracavity fields:
\begin{equation}
	b\up{out} (t) - b\up{in} (t) =  \sqrt{ \kappa } a (t).
\end{equation}



% >>>
